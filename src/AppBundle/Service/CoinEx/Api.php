<?php
namespace AppBundle\Service\CoinEx;

use AppBundle\Service\CoinEx\Endpoint\EndpointInterface;
use AppBundle\Service\CoinEx\Endpoint\MarketList;
use AppBundle\Service\CoinEx\Endpoint\OrderMiningDifficulty;
use AppBundle\Service\CoinEx\Endpoint\MarketDepth;
use AppBundle\Service\CoinEx\Endpoint\MarketKlline;
use AppBundle\Service\CoinEx\Endpoint\OrderLimit;
use AppBundle\Service\CoinEx\Endpoint\OrderMarket;
use AppBundle\Service\CoinEx\Endpoint\OrderPending;
use AppBundle\Service\CoinEx\Endpoint\OrderIoc;
use AppBundle\Service\CoinEx\Endpoint\BalanceInfo;
use AppBundle\Service\CoinEx\Endpoint\OrderUserDeals;
use AppBundle\Service\CoinEx\Endpoint\OrderStatus;
use AppBundle\Service\CoinEx\Endpoint\UnexecutedOrderList;

/**
 * CoinEx Api
 */
class Api
{
    /**
     * Base url
     */
    CONST BASE_URL = 'https://api.coinex.com/v1';
//    CONST BASE_URL = 'http://192.168.214.140/test.php?';
    
    /**
     * Access ID
     * @var string
     */
    private $accessId;
    
    /**
     * Secret Key 
     * @var string
     */
    private $secretKey;

    
    private $logger;

    /**
     * Construct
     * @param string $accessId
     * @param string $secretKey
     */
    public function __construct($accessId, $secretKey, $logger) {
        $this->accessId = $accessId;
        $this->secretKey = $secretKey;
        $this->logger = $logger;
    }
    
    /**
     * Get market list
     */
    public function getMarketList()
    {
        $endpoint = new MarketList();
        $response = $this->request($endpoint);
        
        return $response;
    }
    /**
     * Get market K-Line
     */
    public function getMarketKline(array $params)
    {
        $endpoint = new Endpoint\MarketKline();
        $response = $this->request($endpoint, $params);
        
        return $response;
    }
    
    /**
     * Get Mining Difficulty
     * @return array
     */
    public function getMiningDifficulty()
    {
        $endpoint = new OrderMiningDifficulty();
        $resuponse = $this->request($endpoint);
        
        return $resuponse;
    }
    
    /**
     * getMarketDepth
     * @param array
     */
    public function getMarketDepth(array $params)
    {
        $endpoint = new MarketDepth();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }  
    
    
    /**
     * Limit order
     * @param array $params
     */
    public function limitOrder(array $params)
    {
        $endpoint = new OrderLimit();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    
    /**
     * Limit order
     * @param array $params
     */
    public function marketOrder(array $params)
    {
        $endpoint = new OrderMarket();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    
    
    
    /**
     * Limit order
     * @param array $params
     */
    public function iocOrder(array $params)
    {
        $endpoint = new OrderIoc();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    
    /**
     * Cancel order
     * @param array $params
     */
    public function cancelOrder(array $params)
    {
        $endpoint = new OrderPending();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    /**
     * Cancel order
     * @param array $params
     */
    public function getUnexecutedOrderList(array $params)
    {
        $endpoint = new UnexecutedOrderList();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    /**
     * get order status
     * @param array $params
     */
    public function getOrderStatus(array $params)
    {
        $endpoint = new OrderStatus();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
        
    /**
     * 
     */
    public function getAccountInfo()
    {
        $endpoint = new BalanceInfo();
        $resuponse = $this->request($endpoint);
        
        return $resuponse;
    }
    /**
     * 
     * @param array $params
     */
    public function getUserDeals(array $params)
    {
        
        $endpoint = new OrderUserDeals();
        $resuponse = $this->request($endpoint, $params);
        
        return $resuponse;
    }
    
    /**
     * Rquest
     * @param EndpointInterface $endpoint
     * @param array $params
     */
    private function request(EndpointInterface $endpoint, array $params = [])
    {
        $params['access_id'] = $this->accessId;
        $params['tonce'] = (int)(microtime(true) * 1000);
        $url = sprintf('%s%s', self::BASE_URL, $endpoint->getUrl());
        if (in_array($endpoint->getMethod(), ['GET', 'DELETE'])) {
            $url = sprintf('%s?%s', $url, http_build_query($params));
        }
        $sign = $this->generateSign($params);
        
        $header = [
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
            'authorization: ' . $sign
        ];
        
        $ch = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $header,
        ];
        
        if (in_array($endpoint->getMethod(), ['POST', 'PUT'])) {
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = json_encode($params);
        }
        $options[CURLOPT_CUSTOMREQUEST] = $endpoint->getMethod();
        
//        var_dump($options);
        
	curl_setopt_array($ch, $options);
	$data = curl_exec($ch);
        $info = curl_getinfo($ch);
	curl_close($ch);
        
        $responseData = json_decode($data, true);
        
//        var_dump($info);
//        var_dump($responseData);
        if (in_array($info['http_code'], [504, 500, 502, 524]) || (isset($responseData['code']) && in_array($responseData['code'], [1, 227, 35]))) {
            if (isset($responseData['code'])) {
                $message = sprintf('http_code: %s, api_code: %s, message: %s - %s', $info['http_code'], $responseData['code'], $responseData['message'], json_encode($params));
            } else {
                $message = sprintf('http_code: %s', $info['http_code']);
            }
            $this->logger->error($message);
            usleep(500000);
            $responseData['data'] = $this->request($endpoint, $params);
        } elseif (!isset($responseData['code'])) {
            $message = sprintf('%s : %s - %s', $info['http_code'], $info['url'], json_encode($params));
            $this->logger->error($message);
            throw new \Exception($message);
        } elseif ($responseData['code'] == 0) {
            
        } elseif ($responseData['code'] != 0) {
            $message = $responseData['code'] . ':' . $responseData['message'] . ':' . $url . ':' . json_encode($params);
            if ($responseData['code'] != 600) {
                $this->logger->error($message);
            }
            throw new \Exception($message);
        }
                
	return $responseData['data'];
    }
    
    
    /**
     * 
     * @param array $params
     * @return type
     */
    private function generateSign(array $params)
    {
        // パラメータを並び替え
        ksort($params);
        
        // 最後にシークレットを付ける
        $params['secret_key'] = $this->secretKey;
        
        // パラメータを文字列に
        $string = http_build_query($params);
        
        // md5して大文字へ
        $sign = strtoupper(md5($string));
        
        return $sign;
    }
    
}
