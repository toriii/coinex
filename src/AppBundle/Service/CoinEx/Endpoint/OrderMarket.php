<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderMarket extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/market';
    }
    
    public function getMethod()
    {
        return 'POST';
    }
}