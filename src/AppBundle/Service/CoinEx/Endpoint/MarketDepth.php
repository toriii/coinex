<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class MarketDepth extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/market/depth';
    }
}