<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderStatus extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/status';
    }
}