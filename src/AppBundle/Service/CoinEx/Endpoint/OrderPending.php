<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderPending extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/pending';
    }
    
    public function getMethod()
    {
        return 'DELETE';
    }
}