<?php
namespace AppBundle\Service\CoinEx\Endpoint;

abstract class BaseEndpoint
{
    
    
    /**
     * 
     * @return string
     */
    public function getMethod()
    {
        return 'GET';
    }
    
    /**
     * Get url
     */
    abstract public function getUrl();
}