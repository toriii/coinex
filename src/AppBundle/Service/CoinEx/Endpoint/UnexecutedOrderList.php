<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class UnexecutedOrderList extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/pending';
    }
    
    public function getMethod()
    {
        return 'GET';
    }
}