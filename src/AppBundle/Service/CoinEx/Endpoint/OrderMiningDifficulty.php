<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderMiningDifficulty extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/mining/difficulty';
    }
}