<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderIoc extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/ioc';
    }
    
    public function getMethod()
    {
        return 'POST';
    }
}