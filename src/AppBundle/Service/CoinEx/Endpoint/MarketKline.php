<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class MarketKline extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/market/kline';
    }
}