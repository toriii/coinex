<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class MarketList extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/market/list';
    }
}