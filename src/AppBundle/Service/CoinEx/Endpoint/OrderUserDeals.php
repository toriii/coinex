<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderUserDeals extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/user/deals';
    }
}