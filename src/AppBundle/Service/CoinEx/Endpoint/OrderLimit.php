<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class OrderLimit extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/order/limit';
    }
    
    public function getMethod()
    {
        return 'POST';
    }
}