<?php
namespace AppBundle\Service\CoinEx\Endpoint;
interface EndpointInterface
{
    public function getUrl();
    public function getMethod();
    
}