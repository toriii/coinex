<?php
namespace AppBundle\Service\CoinEx\Endpoint;

class BalanceInfo extends BaseEndpoint implements EndpointInterface
{
    /**
     * Get url
     */
    public function getUrl()
    {
        return '/balance/info';
    }
}