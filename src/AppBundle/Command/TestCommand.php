<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Test
 */
class TestCommand extends BaseBotCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:test')
            ->setDescription('Test')
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $this->outputMarketDepthData('CETBCH');
    }
    
    
}