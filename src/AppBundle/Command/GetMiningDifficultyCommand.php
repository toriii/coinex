<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Test
 */
class GetMiningDifficultyCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:mining-difficulty:get')
            ->setDescription('Trade mining')
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       
        $api = $this->getContainer()->get('coinex_api');
        $updateTime = time();
        
        $delay = 1;
        
        do {
            $cetBchData = $api->getMarketDepth(['market' => 'CETBCH', 'merge' => 0.00000001, 'limit' => 5]);
            $accountInfo = $api->getAccountInfo();
            
            // マイニング状況の確認
            if (time() >= $updateTime) {
                $miningDifficulty = $api->getMiningDifficulty();
                $percentageToUpperLimit = $miningDifficulty['difficulty'] == 0 ? 0 : bcdiv($miningDifficulty['prediction'], $miningDifficulty['difficulty'], 8); 
                $updateTime = ($updateTime == $miningDifficulty['update_time'] + 300) ? (time() + 30)  : $miningDifficulty['update_time'] + 300;

            }
            
            $main = 'BCH';
            $target = 'CET';
            $market = sprintf('%s%s', $target, $main);
            
            $sum = bcadd($accountInfo[$main]['available'], bcmul($accountInfo[$target]['available'], $cetBchData['last'] , 8), 8);
            $mainRate = bcdiv($accountInfo[$main]['available'], $sum, 8) * 100;
            $targetRate = bcdiv(bcmul($accountInfo[$target]['available'], $cetBchData['last'] , 8), $sum, 8) * 100;

            
            
            $output->writeln(sprintf('<info>%s</info>', date('Y-m-d H:i:s')));
            
            // マイニング状況の表示
            $output->writeln(sprintf('<comment>マイニング状況</comment>'));
            $output->writeln(sprintf('  <info>マイ採掘難易度 　　： %.2f CET/毎時</info>', $miningDifficulty['difficulty']));
            $output->writeln(sprintf('  <info>１時間のマイニング ： %.8f CET</info>', $miningDifficulty['prediction']));
            $output->writeln(sprintf('  <info>最終更新時間　　　 ： %s (%.2f%%)</info>', date('Y-m-d H:i:s', $miningDifficulty['update_time']), (date('i', $miningDifficulty['update_time']) / 60 * 100) ));
            
            if ($percentageToUpperLimit > 0.95) {
              $tag  = 'error';
            } elseif($percentageToUpperLimit > 0.9) {
                $tag  = 'comment';
            }else {
                $tag  = 'info';
            }
            $output->writeln(sprintf('  <info>上限までの割合　　 ： </info><%s>%.2f%%</%s>', $tag, $percentageToUpperLimit * 100, $tag));
            
            
            //　板情報を表示
            $output->writeln(sprintf('<comment>CET/BCH板状況</comment>'));
            $output->writeln(sprintf('  <info>買い板: %.8f</info>', $cetBchData['bids'][0][0]));
            $output->writeln(sprintf('  <info>売り板: %.8f</info>', $cetBchData['asks'][0][0]));
            $output->writeln(sprintf('  <info>最終　: %.8f</info>', $cetBchData['last']));     
            
            
            // 残高
            $output->writeln(sprintf('<comment>残高状況</comment>'));
            $output->writeln(sprintf('  <info>%s : %.8f (%.2f%%)</info>', $main, $accountInfo[$main]['available'], $mainRate));
            $output->writeln(sprintf('  <info>%s : %.8f (%.2f%%)</info>', $target, $accountInfo[$target]['available'], $targetRate));
            
            $output->writeln('<info>================================================================================</info>');
            
            sleep($delay);
        } while (true);
        
    }
    
    
}