<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Ranking\RankingData;

/**
 * Test
 */
class GetRankingDataCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:ranking:get')
            ->setDescription('get Ranking data')
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        
        $rankings = (new RankingData())->getRankings();
        
        // パス
        $dir = realpath($this->getContainer()->get('kernel')->getRootDir() . '/../web/ranking');
        
        foreach ($rankings as $rankingId => $ranking) {
            $now = date('Y-m-d H:i:s');

            if (!($ranking['start'] <= $now && $now <= $ranking['end'])) {
                continue;
            }
            
            $output->writeln(sprintf('<comment>Ranking : %d</comment>', $rankingId));
            $url = sprintf('https://www.coinex.com/res/activity/trade/rank/%d', $rankingId);
            $json = file_get_contents($url);
            $data = json_decode($json, true);
            if ($data['code'] != 0) {
                continue;
            }
            $updateTime = date('YmdHis', $data['data']['update_time']);
            $output->writeln(sprintf('  <info>UpdatedAt : %s</info>', date('Y-m-d-H:i:s', $data['data']['update_time'])));
            // ファイル名
            $fileName = sprintf('%d_%s.json', $rankingId, $updateTime);

            // ファイルパス
            $filePath = sprintf('%s/%s',$dir, $fileName);

            if (file_exists($filePath)) {
                continue;
            }
            file_put_contents($filePath, $json);
            $output->writeln(sprintf('  <info>Put : %s</info>', $filePath));
            sleep(3);
        }
        
    }
    
    
}