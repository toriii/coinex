<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * AskBidMineCetCommand
 */
class AskBidMineCet2Command extends ContainerAwareCommand
{
    
    private $market;
    private $marketMain;
    private $marketSub;
    private $orders = [];
    
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:trade:ask-bid-mine-cet-2')
            ->setDescription('Ask, Bid & Mine CET ver.2')
            ->addArgument('market', InputArgument::OPTIONAL, 'Market')
            ->addArgument('orderAmountUsdt', InputArgument::OPTIONAL, 'OrderAmountUsdt')
//            ->addArgument('position', InputArgument::OPTIONAL, 'Position')
            ->addArgument('confirm', InputArgument::OPTIONAL, 'Confirm')
            ->addArgument('difficultyCheck', InputArgument::OPTIONAL, 'Difficulty')
            ->addArgument('ignorePriceBids', InputArgument::OPTIONAL, 'ignorePriceBids')
            ->addArgument('ignorePriceAsks', InputArgument::OPTIONAL, 'ignorePriceAsks')
                
        ;
    }
    
    private function test() 
    {
        $getIgnorePrice = $this->getIgnorePrice('bids');
        var_dump($getIgnorePrice);
        $getIgnorePrice = $this->getIgnorePrice('asks');
        var_dump($getIgnorePrice);
        exit;
    }
    

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $this->api = $this->getContainer()->get('coinex_api');
        $this->helper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        
        $this->delay = 1000000;
        
        // 実行前の設定
        $this->setup();
        
//        $this->test();
        
        
        // 実行前の確認
        if ($this->input->getArgument('confirm') !== '0') {
            $this->confirm();
        }
        // 実行
        while (true) {
            
            // ウォレットを更新
            $this->updateAccountInfo();
            
            // 板情報を取得
            $this->updateMarketDepthData();
        
            $check = bccomp($this->accountInfo['CET']['available'], '0', 8) === 1;
            if ($check) {
                //　注文のチェック（既にある場合）
                $this->checkOrder();

                // 注文
                $this->order();
            }
            
            // 状態の出力
            $this->outputData();
            
            usleep($this->delay);
        }
        
    }
    
    /**
     * 実行前の設定
     */
    private function setup()
    {
        // ペアを選択
        $this->selectMarket();
        
        // 板情報を更新
        $this->updateMarketDepthData();
        
        // ウォレットを更新
        $this->updateAccountInfo();
        
        // ウォレット状況を出力
        $this->outputAccountInfo();
        
        // 注文金額を確認
        $this->setOrderAmountUsdt();
        
    }
    
    /**
     * Select market
     */
    private function selectMarket()
    {
        $argumentMarket = $this->input->getArgument('market');
        $mainList = ['USDT', 'BCH', 'BTC', 'ETH'];
        
        // マーケット一覧を取得
        $marketList = $this->api->getMarketList();
        
        if (in_array($argumentMarket, $marketList)) {
            $this->market = $argumentMarket;
            foreach ($mainList as $val) {
                if (strpos($argumentMarket, $val) !== false) {
                    $this->marketMain = $val;
                    break;
                }
            }
        } else {
            // 売買用通貨を選択
            $marketMainQuestion = new ChoiceQuestion('どのマーケットのペア？', $mainList);
            $this->marketMain = $this->helper->ask($this->input, $this->output, $marketMainQuestion);
            
            // ペアを選択
            $targetMarketList = [];
            foreach ($marketList as $val) {
                if (strpos($val, $this->marketMain) !== false) {
                    $targetMarketList[] = $val;
                }
            }
            $marketQuestion = new ChoiceQuestion('どのペア？', $targetMarketList);
            $this->market = $this->helper->ask($this->input, $this->output, $marketQuestion);
        }
        $this->marketSub = str_replace($this->marketMain, '', $this->market);
    }
    
    /**
     * Update Market Depth Data
     */
    private function updateMarketDepthData()
    {
        
        // 取引を行うペアの板
        $this->marketDepthData[$this->market] = $this->api->getMarketDepth(['market' => $this->market, 'merge' => 0.00000001, 'limit' => 5]);
        if ($this->marketMain !== 'USDT') {
            // 取引通貨とドルのペア情報
            $this->marketDepthData[$this->marketMain . 'USDT'] = $this->api->getMarketDepth(['market' => $this->marketMain. 'USDT', 'merge' => 0.00000001, 'limit' => 5]);
        }
    }
    
    /**
     * Update account info
     */
    private function updateAccountInfo()
    {
        $this->accountInfo = $this->api->getAccountInfo();
    }
    
    /**
     * Output account info
     */
    private function outputAccountInfo()
    {
        $this->output->writeln(sprintf('<comment>残高状況</comment>'));
        if ($this->marketMain === 'USDT') {
            $this->output->writeln(sprintf('  <info>%5s : %.8f</info>', $this->marketMain, $this->accountInfo[$this->marketMain]['available']));
        } else {
            $balance = $this->accountInfo[$this->marketMain]['available'];
            $this->output->writeln(sprintf('  <info>%5s : %.8f (%.8f USDT)</info>', $this->marketMain, $balance, $this->getMainBalanceUsdt()));
        }
        $this->output->writeln(sprintf('  <info>%5s : %.8f (%.8f USDT)</info>', $this->marketSub, $this->accountInfo[$this->marketSub]['available'], $this->getSubBalanceUsdt()));
    }
    
    /**
     * Get main balance usdt
     */
    private function getMainBalanceUsdt($mainAmount = null) {
        if ($mainAmount === null) {
            $mainAmount = $this->accountInfo[$this->marketMain]['available'];
        }
        if ($this->marketMain === 'USDT') {
            $usdtAmount = $mainAmount;
        } else {
            $usdtAmount = bcmul($mainAmount, $this->marketDepthData[$this->marketMain . 'USDT']['last'], 8);
        }
        return $usdtAmount;
    }
    
    /**
     * Get main balance usdt
     */
    private function getSubBalanceUsdt($subAmount = null, $price = null) {
        if ($subAmount === null) {
            $subAmount = $this->accountInfo[$this->marketSub]['available'];
        }
        if ($price === null) {
            $price = $this->marketDepthData[$this->market]['last'];
        }
        $subToMain = bcmul($subAmount, $price, 8);
        
        $usdtAmount = $this->getMainBalanceUsdt($subToMain);
        
        
        return $usdtAmount;
    }
    
    /**
     * Set order amount usdt
     */
    private function setOrderAmountUsdt()
    {
        $this->orderAmountUsdt = $this->input->getArgument('orderAmountUsdt');
        if (empty($this->orderAmountUsdt)) {
            $this->orderAmountUsdt = 110;
        } 
//        if ($this->orderAmountUsdt < 100) {
//            $this->output->writeln(sprintf('<error>100ドル以上が「Ask, Bid & Mine CET」の対象になります。</error>'));
//            exit;
//        }
    }
    
    
    /**
     * Confirm
     */
    private function confirm()
    {
        $this->output->writeln('<comment>========================================</comment>');
        $this->output->writeln('<info>下記の内容で5秒後に開始します。</info>');
        $this->output->writeln('<info>----------------------------------------</info>');
        $this->output->writeln(sprintf('<info>ペア : %s/%s</info>', $this->marketMain, $this->marketSub));
        $this->output->writeln(sprintf('<info>注文 : %d USDT</info>', $this->orderAmountUsdt));
//        $this->output->writeln(sprintf('<info>位置 : %d</info>', $this->position));
        $this->output->writeln('<comment>========================================</comment>');
        
        for ($i = 5; $i > 0; $i--) {
            sleep(1);
            $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■',$i)));
        }
        
    }
    
    /**
     * 注文処理
     */
    private function order()
    {
        // 買い注文
        $buyPrice = $this->getOrderPrice('bids');
        
        // 注文回数
        $numBuy = $this->getOrderNum('buy', $buyPrice);
        
        // 注文量
        $buyAmount = $this->getOrderAmount($buyPrice);
        
        // 注文ループ
        for ($i = 0; $i < $numBuy; $i++) {
            // 注文APIにリクエスト
            try {
                $orderResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'buy', 'amount' => $buyAmount, 'price' => $buyPrice]);
                $this->orders[$orderResponse['id']] = $orderResponse;
            } catch (\Exception $ex) {
            }
            usleep(500000);
        }
        
        // 売り注文
        $sellPrice = $this->getOrderPrice('asks');
        
        // 注文回数
        $numsell = $this->getOrderNum('sell', $sellPrice);
        
        // 注文量
        $sellAmount = $this->getOrderAmount($sellPrice);
        
        // 注文ループ
        for ($i = 0; $i < $numsell; $i++) {
            // 注文APIにリクエスト
            try {
                $orderResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'sell', 'amount' => $sellAmount, 'price' => $sellPrice]);
                $this->orders[$orderResponse['id']] = $orderResponse;
            } catch (\Exception $ex) {
            }
            usleep(200000);
        }       
        
        
    }
    
    
    
    /**
     * 注文数を取得
     * Get order price
     */
    private function getOrderNum($type, $price)
    {
        if ($type === 'buy') {
            // 最大の買い注文量
            $amountToBuy = bcdiv($this->accountInfo[$this->marketMain]['available'], $price, 8);
            $usdtToBuy = $this->getSubBalanceUsdt($amountToBuy, $price);
            $num = (int)bcdiv($usdtToBuy, $this->orderAmountUsdt, 8);
        } else {
            // 最大の売り量
            $amountToSell = $this->accountInfo[$this->marketSub]['available'];
            $usdtToSell = $this->getSubBalanceUsdt($amountToSell, $price);
            $num = (int)bcdiv($usdtToSell, $this->orderAmountUsdt, 8);
        }
        
        
        return $num;
    }
    
    
    /**
     * 注文量を取得
     * Get order price
     */
    private function getOrderAmount($price)
    {
        if ($this->marketMain === 'USDT') {
            $amount = bcdiv($this->orderAmountUsdt, $price, 8);
        } else {
            $orderPriceSum = bcdiv($this->orderAmountUsdt, $this->marketDepthData[$this->marketMain . 'USDT']['last'], 8);
            $amount = bcdiv($orderPriceSum, $price, 8);
        }
        
        return $amount;
    }
    
    
    /**
     * 注文価格を取得
     * Get order price
     */
    private function getOrderPrice($type, $orderCheck = true)
    {
        if (!in_array($type, ['bids', 'asks'])) {
            throw new \Exception('種別がおかしい');
        }
        
        $reverseType = ($type === 'bids') ? 'asks' : 'bids';
        
        $basePrice = $this->marketDepthData[$this->market][$type][0][0];
        $this->basePrice[$type] = $basePrice;
        
        // 100ドル超えてるかチェック
        $basePriceAmountUsdt = $this->getSubBalanceUsdt($this->marketDepthData[$this->market][$type][0][1], $this->marketDepthData[$this->market][$type][0][0]);
                
        // 有利な価格
        
        if ($type === 'bids') {
            $tmpPrice = bcadd($basePrice, bcmul('0.00000001', '1', 8), 8);
        } else {
            $tmpPrice = bcsub($basePrice, bcmul('0.00000001', '1', 8), 8);
        }
        $this->tmpPrice[$type] = $tmpPrice;
        
        // 逆注文の価格もチェック
        $reversePriceCheck = bccomp($this->marketDepthData[$this->market][$reverseType][0][0], $tmpPrice, 8);
        $ignorePrice = $this->getIgnorePrice($type);
        if (($reversePriceCheck === 0) || ($basePriceAmountUsdt < $ignorePrice)) {
            $price = $basePrice;
        } else {
            $price = $tmpPrice;
        }
        
        
        $difficultyCheckArgument = $this->input->getArgument('difficultyCheck');
        if ($difficultyCheckArgument) {
            $miningDifficulty = $this->api->getMiningDifficulty();
            $percentageToUpperLimit = $miningDifficulty['difficulty'] == 0 ? 0 : bcdiv($miningDifficulty['prediction'], $miningDifficulty['difficulty'], 8); 
 
            $difficultyCheck = ($percentageToUpperLimit > 0.9);
            if ($difficultyCheck) { // 一定を越えていたら先頭には出さない
                $price = $this->marketDepthData[$this->market][$type][$difficultyCheckArgument - 1][0];
            }
        }
        
        
        // 既にある自分注文の価格もチェック
        $orderType = ($type === 'bids') ? 'buy' : 'sell';
        $bestOrderPrice = null;
        foreach ($this->orders as $order) {
            if ($order['type'] === $orderType) {
                if ($bestOrderPrice === null || ($orderType == 'buy' && bccomp($order['price'], $bestOrderPrice, 8) === 1)) {
                    $bestOrderPrice = $order['price'];
                } else if ($bestOrderPrice === null || ($orderType == 'buy' && bccomp($bestOrderPrice, $order['price'], 8) === 1)) {
                    $bestOrderPrice = $order['price'];
                }
            }
        }
        if ($bestOrderPrice !== null && bccomp($this->marketDepthData[$this->market][$type][0][0], $bestOrderPrice, 8) === 0) {
            $price = $bestOrderPrice;
        }
        $this->lastPrice[$type] = $price;
        
        return $price;
    }

    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->delay / 1000000)));
        
        // 板
        $this->output->writeln(sprintf('<comment>%s板状況</comment>', $this->market));
        for ($i = 4; $i >= 0; $i--) {
            $usdt = $this->getSubBalanceUsdt($this->marketDepthData[$this->market]['asks'][$i][1], $this->marketDepthData[$this->market]['asks'][$i][0]);
            $this->output->writeln(sprintf('  <info>%.8f - %.4f (%.2f USDT)</info>', $this->marketDepthData[$this->market]['asks'][$i][0], $this->marketDepthData[$this->market]['asks'][$i][1], $usdt));
        }
        $this->output->writeln('<info>--------------------</info>');
        $this->output->writeln(sprintf('  <info>%.8f</info>', $this->marketDepthData[$this->market]['last']));   
        $this->output->writeln('<info>--------------------</info>');
        for ($i = 0; $i <= 4; $i++) {
            $usdt = $this->getSubBalanceUsdt($this->marketDepthData[$this->market]['bids'][$i][1], $this->marketDepthData[$this->market]['bids'][$i][0]);
            $this->output->writeln(sprintf('  <info>%.8f - %.4f (%.2f USDT)</info>', $this->marketDepthData[$this->market]['bids'][$i][0], $this->marketDepthData[$this->market]['bids'][$i][1], $usdt));
        }
        
        // 現在の注文
        $this->output->writeln(sprintf('<comment>現在の注文状況</comment>'));
        if (!empty($this->orders)) {
            foreach ($this->orders as $order) {
                $this->output->writeln(sprintf('  <info>%.8f - %.8f - %s - %s</info>', $order['price'], $order['amount'], $order['type'], date('Y-m-d H:i:s', $order['create_time'])));
            }
        } else {
            $this->output->writeln('  <info>なし</info>');
        }
        
        // 残高
        $this->outputAccountInfo();
        
        
        // 注文価格の計算
        $this->output->writeln(sprintf('<comment>注文価格</comment>'));
        $this->output->writeln('  <info>買い注文</info>');
        $this->output->writeln(sprintf('    <info>基準（最前列）: %.8f</info>', $this->basePrice['bids']));
        $this->output->writeln(sprintf('    <info>有利（追抜後）: %.8f</info>', $this->tmpPrice['bids']));
        $this->output->writeln(sprintf('    <info>最終（調整後）: %.8f</info>', $this->lastPrice['bids']));
        $this->output->writeln('  <info>売り注文</info>');
        $this->output->writeln(sprintf('    <info>基準（最前列）: %.8f</info>', $this->basePrice['asks']));
        $this->output->writeln(sprintf('    <info>有利（追抜後）: %.8f</info>', $this->tmpPrice['asks']));
        $this->output->writeln(sprintf('    <info>最終（調整後）: %.8f</info>', $this->lastPrice['asks']));
        
        
        
        
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    /**
     * Check order
     */
    private function checkOrder()
    {
        // 注文データを取得
        $this->getOrders();
        
        // 注文がなければ何もしない
        if (empty($this->orders)) {
            return;
        }
        
        foreach ($this->orders as $key => $order) {
            $cancel = false;
            
            // 一部でも約定している場合
            if ($order['status'] !== 'not_deal') {
                $cancel = true;
            }
            $marketType = ($order['type'] === 'buy') ? 'bids' : 'asks';
            $bestPrice = $this->getOrderPrice($marketType, false);
            
            
            if (bccomp($bestPrice, $order['price'], 8) !== 0) {
                $cancel = true;
            }
            
            // キャンセル
            if ($cancel) {
                try {
                    $this->api->cancelOrder(['market' => $this->market, 'id' => $order['id']]);
                } catch (\Exception $ex) {
                }
                unset($this->orders[$key]);
                usleep(200000);
            }
        }
        
    }
    
    /**
     * 現在の注文を取得
     */
    private function getOrders() {
        $data = $this->api->getUnexecutedOrderList(['market' => $this->market, 'limit' => 100, 'page' => 1]);
        $this->orders = $data['data'];
    }
    
    
    private function getIgnorePrice($type)
    {
        if ($type === 'bids') {
            $ignorePrice = (int)$this->input->getArgument('ignorePriceBids');
        } else {
            $ignorePrice = (int)$this->input->getArgument('ignorePriceAsks');
        }
        if (empty($ignorePrice)) {
            $ignorePrice = 100;
        }
        
        return $ignorePrice;
    }
    
    
}