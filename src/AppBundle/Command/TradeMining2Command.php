<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Test
 */
class TradeMining2Command extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:trade:mining-2')
            ->setDescription('Trade mining2')
            ->addArgument('market', InputArgument::OPTIONAL, 'Market')
            ->addArgument('useCetAsFees', InputArgument::OPTIONAL, 'Use CET as Fees')
            ->addArgument('delay', InputArgument::OPTIONAL, 'sleep')
            ->addArgument('merge', InputArgument::OPTIONAL, 'merge')
            ->addArgument('reserve', InputArgument::OPTIONAL, 'reserve')
                
        ;
    }


    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->api = $this->getContainer()->get('coinex_api');
        $this->helper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
       
        
        // 実行前の設定
        $this->setup();
        
        
        
        do {
            
            // 難易度の更新
            $this->updateMiningDifficultyData();
            
            // ウォレットを更新
            $this->updateAccountInfo();
            
            // ペアの切り替え
            $this->changeMarket();
            
            // 板情報を取得
            $this->updateMarketDepthData();
            
            // 停止中か判別
            $isStop = $this->isStop();
            
            if (!$isStop) {
                // 同値取引
                $this->trade();
                $this->updateAccountInfo();
            }
            
            // 残高の調整
            $this->adjustment();
            
            // 状態の出力
            $this->outputData();
            
            usleep($this->delay);
        } while(true);
        
        
    }
    
    
    
    /**
     * 実行前の設定
     */
    private function setup()
    {
        // ペアを選択
        $this->selectMarket();
        
        // 更新間隔をセット
        $this->setDelay();
        
        // マイニング難易度のチェック時間
        $this->updateTime = time();
        
        // CET手数料
        $this->useCetAsFees = (boolean)$this->input->getArgument('useCetAsFees');

        $this->setMerge();
        $this->miningLimit = 0.95;
    }
    
    private function setMerge()
    {
        $merge = $this->input->getArgument('merge');
        if (!empty($merge)) {
            $this->merge = $merge;
        } else {
            $this->merge = 8;
        }
    }
    
    /**
     * 実行間隔をセット
     */
    private function setDelay()
    {
        $delay = (float)$this->input->getArgument('delay');
        $this->delay = ($delay >= 0.5 && $delay <=5) ? ($delay * 1000000) : 1000000;  
    }
    
    /**
     * 秒で実行間隔を取得
     */
    private function getDelayM()
    {
        return bcdiv($this->delay, 1000000, 2);
    }
    
    
    /**
     * 秒で実行間隔を取得
     */
    private function getMarketList()
    {
        if (empty($this->marketList)) {
            $this->marketList = $this->api->getMarketList();
        }
        
        return $this->marketList;
    }
    
    /**
     * Marketを選択
     * Select market
     */
    private function selectMarket()
    {
        $argumentMarket = $this->input->getArgument('market');
        $mainList = ['USDT', 'BCH', 'BTC', 'ETH', 'CET'];
        
        // マーケット一覧を取得
        $marketList = $this->getMarketList();
        
        if (in_array($argumentMarket, $marketList)) {
            $this->market = $argumentMarket;
            foreach ($mainList as $val) {
                if (strpos($argumentMarket, $val) !== false) {
                    $this->marketMain = $val;
                    break;
                }
            }
        } else {
            // 売買用通貨を選択
            $marketMainQuestion = new ChoiceQuestion('どのマーケットのペア？', $mainList);
            $this->marketMain = $this->helper->ask($this->input, $this->output, $marketMainQuestion);
            
            // ペアを選択
            $targetMarketList = [];
            foreach ($marketList as $val) {
                if (strpos($val, $this->marketMain) !== false) {
                    $targetMarketList[] = $val;
                }
            }
            $marketQuestion = new ChoiceQuestion('どのペア？', $targetMarketList);
            $this->market = $this->helper->ask($this->input, $this->output, $marketQuestion);
        }
        $this->marketSub = str_replace($this->marketMain, '', $this->market);
    }
    
    /**
     * 板情報取得
     * Update Market Depth Data
     */
    private function updateMarketDepthData()
    {
        
        // 取引を行うペアの板
        $this->marketDepthData[$this->market] = $this->api->getMarketDepth(['market' => $this->market, 'merge' => 0.00000001, 'limit' => 5]);
        // CET とのペアもとる
        if ($this->marketMain !== 'CET' && (empty($this->marketDepthData['CET'. $this->marketMain]) || (date('s') % 10 == 0))) {
            $this->marketDepthData['CET'. $this->marketMain] = $this->api->getMarketDepth(['market' => 'CET'. $this->marketMain, 'merge' => 0.00000001, 'limit' => 5]);
        }
    }
    
    
    /**
     * 残高を更新
     * Update account info
     */
    private function updateAccountInfo()
    {
        $this->accountInfo = $this->api->getAccountInfo();
    }
    
    /**
     * 残高を出力
     * Output account info
     */
    private function outputAccountInfo()
    {
        if (!isset($this->mainRate)) {
            $this->mainRate = null;
        }
        if (!isset($this->subRate)) {
            $this->subRate = null;
        }
        
        $this->output->writeln(sprintf('<comment>残高状況</comment>'));
        $this->output->writeln(sprintf('  <info>%s : %.8f (%.2f %%)</info>', $this->marketMain, $this->accountInfo[$this->marketMain]['available'], $this->mainRate));
        $this->output->writeln(sprintf('  <info>%s : %.8f (%.2f %%)</info>', $this->marketSub, $this->accountInfo[$this->marketSub]['available'], $this->subRate));
    }
    
    /**
     * 難易度データを更新
     */
    private function updateMiningDifficultyData()
    {
        if (time() >= $this->updateTime) {
            $this->miningDifficulty = $this->api->getMiningDifficulty();
            $this->percentageToUpperLimit = $this->miningDifficulty['difficulty'] == 0 ? 0 : bcdiv($this->miningDifficulty['prediction'], $this->miningDifficulty['difficulty'], 8); 
            $this->updateTime = ($this->updateTime == $this->miningDifficulty['update_time'] + 30) ? (time() + 10)  : $this->miningDifficulty['update_time'] + 30;
        }
    }
    
    
    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->getDelayM())));
        
        
        // マイニング状況の表示
        $this->output->writeln(sprintf('<comment>マイニング状況</comment>'));
        $this->output->writeln(sprintf('  <info>マイ採掘難易度 　　： %.2f CET/毎時</info>', $this->miningDifficulty['difficulty']));
        $this->output->writeln(sprintf('  <info>１時間のマイニング ： %.8f CET</info>', $this->miningDifficulty['prediction']));
        $this->output->writeln(sprintf('  <info>最終更新時間　　　 ： %s (%.2f%%)</info>', date('Y-m-d H:i:s', $this->miningDifficulty['update_time']), (date('i', $this->miningDifficulty['update_time']) / 60 * 100) ));
        $this->output->writeln(sprintf('  <info>上限までの割合　　 ： %.2f%%</info>', $this->percentageToUpperLimit * 100));
        
        // 板
        $this->output->writeln(sprintf('<comment>%s板状況</comment>', $this->market));
        $this->output->writeln(sprintf('  <info>買い板: %.8f</info>', $this->marketDepthData[$this->market]['bids'][0][0]));
        $this->output->writeln(sprintf('  <info>売り板: %.8f</info>', $this->marketDepthData[$this->market]['asks'][0][0]));
        $this->output->writeln(sprintf('  <info>最終　: %.8f</info>', $this->marketDepthData[$this->market]['last']));   
        
        // 残高
        $this->outputAccountInfo();
        
        
        $this->output->writeln(sprintf('<comment>その他</comment>'));
        if (!empty($this->messages)) {
            foreach ($this->messages as $message) {
                $this->output->writeln(sprintf('  <info>%s</info>', $message));
            }
        } else {
            $this->output->writeln('  <info>なし</info>');   
        }
        
        $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■', date('s'))));
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    /**
     * 注文停止中かの判別
     */
    private function isStop()
    {
        $this->messages = [];
        $isStop = false;
        
        // 難易度 0
        if ($this->miningDifficulty['difficulty'] == 0) {
            $this->messages[] = 'マイ採掘難易度が0です。（09:00過ぎの場合は反映待ち？）';
            $isStop = true;
        }
        
        // スプレッドなし
        $difference = bcsub($this->marketDepthData[$this->market]['asks'][0][0], $this->marketDepthData[$this->market]['bids'][0][0], 8);
        $check = '0.' . str_repeat('0', ($this->merge - 1)) . '1';
        if (bccomp($difference, $check, 8) === 0) {
            $this->messages[] = '板ぴったり...。';
            $isStop = true;
        }
        
        // 難易度ギリギリ
        if ($this->percentageToUpperLimit >= $this->miningLimit) {
            $this->messages[] = sprintf('%.2f%%以上のため%sまで停止', $this->miningLimit * 100, date('H:00', strtotime('+1 hour')));
            $isStop = true;
        }
        
        return $isStop;
    }
    
    /**
     * 同値取引
     */
    private function trade()
    {
        $price = $this->getOrderPrice();
        $amount = $this->getOrderAmount($price);
        
        // 注文
        try {
            $sellResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'sell', 'amount' => $amount, 'price' => $price]);
        } catch (\Exception $ex) {
        }
        
        try {
            $buyResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'buy', 'amount' => $amount, 'price' => $price]);
        } catch (\Exception $ex) {
        }
                
                
        // キャンセル
        try {
            if (!empty($sellResponse['id'])) {
                $scor = $this->api->cancelOrder(['market' => $this->market, 'id' => $sellResponse['id']]);
            }
        } catch (\Exception $ex) {
        }
        try {
            if (!empty($buyResponse['id'])) {
                $bcor = $this->api->cancelOrder(['market' => $this->market, 'id' => $buyResponse['id']]);
            }
        } catch (\Exception $ex) {
        }
    }
    
    
    /**
     * 注文金額を取得
     */
    private function getOrderPrice()
    {
        $difference = bcsub($this->marketDepthData[$this->market]['asks'][0][0], $this->marketDepthData[$this->market]['bids'][0][0], 8);
        $price = bcadd($this->marketDepthData[$this->market]['bids'][0][0], bcdiv($difference, 2, 8), 8);
        
        return $price;
    }
    
    /**
     * 注文量を取得
     */
    private function getOrderAmount($price)
    {
        $remaining = bcsub($this->miningDifficulty['difficulty'], $this->miningDifficulty['prediction'], 8);  // マイニング上限までの残り
        $remainingTime = ((60 - date('i')) * 60) - date('s'); // 残り時間（秒）
        $cetFeeOfOneSecond = $remaining / $remainingTime; // 1秒で必要な手数料CET
        
        if ($this->marketMain == 'CET') {
            $mainFeeOfOneSecond = $cetFeeOfOneSecond;
        } else {
            
            $cetMainPrice = $this->marketDepthData['CET'. $this->marketMain]['last'];
            $mainFeeOfOneSecond = bcmul($cetFeeOfOneSecond, $cetMainPrice, 8);
        }
        
        // 補正値
        if ($this->percentageToUpperLimit < 0.5 && date('i') < 20) {
            $correctionValue = 5;
        } elseif ($this->percentageToUpperLimit < 0.7 && date('i') < 30) {
           $correctionValue = 4;
        } elseif ($this->percentageToUpperLimit < 0.8 && date('i') < 45) {
           $correctionValue = 2;
        } elseif ($this->percentageToUpperLimit < 0.85 && date('i') < 50) {
            $correctionValue = 1.5;
        } elseif ($this->percentageToUpperLimit < 0.9 && date('i') < 55) {
            $correctionValue = 1;
        } else {
           $correctionValue = 0.2;
        }
        
        // 実行間隔での補正
        $delaym = $this->getDelayM();
        if ($delaym < 1) {
            $delaym = $delaym * 1.5; 
        }
        $correctionValue = $correctionValue * $delaym;
        
        // CET手数料の場合の補正
        if ($this->useCetAsFees) {
            $correctionValue = $correctionValue * 2;
        }
        
        // 基準となる取引量
        $baseAmount = bcmul(bcdiv($mainFeeOfOneSecond, $price, 8), 500, 8);

        // 補正ありの取引量
        $amount = bcmul($baseAmount, (string)$correctionValue, 8); // 1秒に必要な取引量
        
        // 残高による調整
        if (bccomp($this->accountInfo[$this->marketSub]['available'], $amount, 8) === -1) {
            $amount = bcmul($this->accountInfo[$this->marketSub]['available'], 0.8, 8);
        }
        $priceSum = bcmul($price, $amount, 8);
        if (bccomp($this->accountInfo[$this->marketMain]['available'], $priceSum, 8) === -1) {
            $amount = bcmul(bcdiv($this->accountInfo[$this->marketMain]['available'], $price, 8), 0.8, 8);
        }

        return $amount;
    }
    
    /**
     * 残高調整
     */
    private function adjustment()
    {
        // 難易度状況チェック
        if ($this->percentageToUpperLimit >= $this->miningLimit) {
            return;
        }
        $price = $this->marketDepthData[$this->market]['last'];
        $sum = bcadd($this->accountInfo[$this->marketMain]['available'], bcmul($this->accountInfo[$this->marketSub]['available'], $price , 8), 8);
        $mainRate = bcdiv($this->accountInfo[$this->marketMain]['available'], $sum, 8) * 100;
        $targetRate = bcdiv(bcmul($this->accountInfo[$this->marketSub]['available'], $price , 8), $sum, 8) * 100;
        
        $this->mainRate = $mainRate;
        $this->subRate = $targetRate;
        
                
        $adjustmentType = ($mainRate > $targetRate) ? 'buy' : 'sell';
        $adjustmentDiff = abs(50 - $targetRate) * 0.01;
        $targetAmountSum = bcadd(bcdiv($this->accountInfo[$this->marketMain]['available'], $price, 8), $this->accountInfo[$this->marketSub]['available'], 8); // 対象側換算の総量
            
        if ($adjustmentDiff > 0.03) {
            $adjustmentAmount = ($adjustmentDiff > 0.2) ? sprintf('%.8f', $targetAmountSum * 0.05) : sprintf('%.8f', $targetAmountSum * 0.01);
            $adjustmentPrice = ($mainRate > $targetRate) ? $this->marketDepthData[$this->market]['asks'][0][0] : $this->marketDepthData[$this->market]['bids'][0][0];
            $this->api->iocOrder(['market' => $this->market, 'type' => $adjustmentType, 'amount' => $adjustmentAmount, 'price' => $adjustmentPrice]);
        }
    }
    
    /**
     * ペアの切り替え
     */
    private function changeMarket()
    {
        $argumentMarket = $this->input->getArgument('reserve');
        
        // 指定がなければ何もしない
        if (empty($argumentMarket)) {
            return ;
        }
        
        // 条件満たしていれば切り替える
        if (($this->percentageToUpperLimit < 0.9) && (date('i') >= 50) || ($this->percentageToUpperLimit < 0.7) && (date('i') >= 45)) {
            if ($argumentMarket == $this->market) {
                // 既に変わって入れば何もしない
                return;
            }
            $mainList = ['USDT', 'BCH', 'BTC', 'ETH', 'CET'];

            // マーケット一覧を取得
            $marketList = $this->getMarketList();

            if (in_array($argumentMarket, $marketList)) {
                $this->market = $argumentMarket;
                foreach ($mainList as $val) {
                    if (strpos($argumentMarket, $val) !== false) {
                        $this->marketMain = $val;
                        break;
                    }
                }
            } else {
                throw new \Exception("Market Not found");
            }
            $this->marketSub = str_replace($this->marketMain, '', $this->market);
            if ($this->marketMain === 'CET') {
                $this->merge = 4;
            } else {
                $this->merge = 8;
            }
        } elseif ($argumentMarket == $this->market) {
            // 本来のに戻す
            $this->selectMarket();
            $this->setMerge();
        }

    }
    
    
}