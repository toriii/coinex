<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * AskBidMineCetCommand
 */
class AskBidMineCetCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:trade:ask-bid-mine-cet')
            ->setDescription('Ask, Bid & Mine CET')
            ->addArgument('market', InputArgument::OPTIONAL, 'Market')
            ->addArgument('orderAmountUsdt', InputArgument::OPTIONAL, 'OrderAmountUsdt')
            ->addArgument('position', InputArgument::OPTIONAL, 'Position')
            ->addArgument('confirm', InputArgument::OPTIONAL, 'Confirm')
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $this->api = $this->getContainer()->get('coinex_api');
        $this->helper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;
        
        $this->delay = 1000000;
        
        // 実行前の設定
        $this->setup();
        
        // 実行前の確認
        if ($this->input->getArgument('confirm') !== '0') {
            $this->confirm();
        }
        // 実行
        while (true) {
            // 板情報を取得
            $this->updateMarketDepthData();
            
            // ウォレットを更新
            $this->updateAccountInfo();
        
            //　注文のチェック（既にある場合）
            $this->checkOrder();
            
            // Subを処分（持っていれば）
            $this->sellSub();
            
            // 注文
            $this->order();
            
            // 状態の出力
            $this->outputData();
            
            usleep($this->delay);
        }
        
    }
    
    /**
     * 実行前の設定
     */
    private function setup()
    {
        // ペアを選択
        $this->selectMarket();
        
        // 板情報を更新
        $this->updateMarketDepthData();
        
        // ウォレットを更新
        $this->updateAccountInfo();
        
        // ウォレット状況を出力
        $this->outputAccountInfo();
        
        // 注文金額を確認
        $this->setOrderAmountUsdt();
        
        // 並べる位置を指定
        $this->setPosition();
    }
    
    /**
     * Select market
     */
    private function selectMarket()
    {
        $argumentMarket = $this->input->getArgument('market');
        $mainList = ['USDT', 'BCH', 'BTC', 'ETH'];
        
        // マーケット一覧を取得
        $marketList = $this->api->getMarketList();
        
        if (in_array($argumentMarket, $marketList)) {
            $this->market = $argumentMarket;
            foreach ($mainList as $val) {
                if (strpos($argumentMarket, $val) !== false) {
                    $this->marketMain = $val;
                    break;
                }
            }
        } else {
            // 売買用通貨を選択
            $marketMainQuestion = new ChoiceQuestion('どのマーケットのペア？', $mainList);
            $this->marketMain = $this->helper->ask($this->input, $this->output, $marketMainQuestion);
            
            // ペアを選択
            $targetMarketList = [];
            foreach ($marketList as $val) {
                if (strpos($val, $this->marketMain) !== false) {
                    $targetMarketList[] = $val;
                }
            }
            $marketQuestion = new ChoiceQuestion('どのペア？', $targetMarketList);
            $this->market = $this->helper->ask($this->input, $this->output, $marketQuestion);
        }
        $this->marketSub = str_replace($this->marketMain, '', $this->market);
    }
    
    /**
     * Update Market Depth Data
     */
    private function updateMarketDepthData()
    {
        
        // 取引を行うペアの板
        $this->marketDepthData[$this->market] = $this->api->getMarketDepth(['market' => $this->market, 'merge' => 0.00000001, 'limit' => 5]);
        if ($this->marketMain !== 'USDT') {
            // 取引通貨とドルのペア情報
            $this->marketDepthData[$this->marketMain . 'USDT'] = $this->api->getMarketDepth(['market' => $this->marketMain. 'USDT', 'merge' => 0.00000001, 'limit' => 5]);
        }
    }
    
    /**
     * Update account info
     */
    private function updateAccountInfo()
    {
        $this->accountInfo = $this->api->getAccountInfo();
    }
    
    /**
     * Output account info
     */
    private function outputAccountInfo()
    {
        $this->output->writeln(sprintf('<comment>残高状況</comment>'));
        if ($this->marketMain === 'USDT') {
            $this->output->writeln(sprintf('  <info>%5s : %.8f</info>', $this->marketMain, $this->accountInfo[$this->marketMain]['available']));
        } else {
            $balance = $this->accountInfo[$this->marketMain]['available'];
            $this->output->writeln(sprintf('  <info>%5s : %.8f (%.8f USDT)</info>', $this->marketMain, $balance, $this->getMainBalanceUsdt()));
        }
        $this->output->writeln(sprintf('  <info>%5s : %.8f</info>', $this->marketSub, $this->accountInfo[$this->marketSub]['available']));
    }
    
    /**
     * Get main balance usdt
     */
    private function getMainBalanceUsdt() {
        $mainAmount = $this->accountInfo[$this->marketMain]['available'];
        if ($this->marketMain === 'USDT') {
            $usdtAmount = $mainAmount;
        } else {
            $usdtAmount = bcmul($mainAmount, $this->marketDepthData[$this->marketMain . 'USDT']['last'], 8);
        }
        return $usdtAmount;
    }
    
    /**
     * Set order amount usdt
     */
    private function setOrderAmountUsdt()
    {
        $this->orderAmountUsdt = $this->input->getArgument('orderAmountUsdt');
        if (empty($this->orderAmountUsdt)) {
            $this->orderAmountUsdt = 110;
        } 
        if ($this->orderAmountUsdt < 100) {
            $this->output->writeln(sprintf('<error>100ドル以上が「Ask, Bid & Mine CET」の対象になります。</error>'));
            exit;
        }
        
        if (bccomp($this->orderAmountUsdt, $this->getMainBalanceUsdt(), 8) === 1) {
            $this->output->writeln(sprintf('<error>%sの残高が不足しています。</error>', $this->marketMain));
            exit;
        }
    }
    
    /**
     * Set position
     */
    private function setPosition()
    {
        $this->position = (int)$this->input->getArgument('position');
        if (empty($this->position)) {
            $this->position = 0;
        }
        if ($this->position < -1 || $this->position > 10) {
            $this->output->writeln('<error>並び位置の指定は-1~10にしてください。</error>');
        }
        
        
    }
    
    /**
     * Confirm
     */
    private function confirm()
    {
        $this->output->writeln('<comment>========================================</comment>');
        $this->output->writeln('<info>下記の内容で5秒後に開始します。</info>');
        $this->output->writeln('<info>----------------------------------------</info>');
        $this->output->writeln(sprintf('<info>ペア : %s/%s</info>', $this->marketMain, $this->marketSub));
        $this->output->writeln(sprintf('<info>注文 : %d USDT</info>', $this->orderAmountUsdt));
        $this->output->writeln(sprintf('<info>位置 : %d</info>', $this->position));
        $this->output->writeln('<comment>========================================</comment>');
        
        for ($i = 5; $i > 0; $i--) {
            sleep(1);
            $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■',$i)));
        }
        
    }
    
    /**
     * 注文処理
     */
    private function order()
    {
        // 板に並べてあれば注文しない
        if (!empty($this->lastOrder)) {
            return;
        }
        
        // 注文価格
        $price = $this->getOrderPrice();
        
        // 注文量
        if ($this->marketMain === 'USDT') {
            $amount = bcdiv($this->orderAmountUsdt, $price, 8);
        } else {
            $orderPriceSum = bcdiv($this->orderAmountUsdt, $this->marketDepthData[$this->marketMain . 'USDT']['last'], 8);
            $amount = bcdiv($orderPriceSum, $price, 8);
        }
        
        // 注文APIにリクエスト
        try {
            $orderResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'buy', 'amount' => $amount, 'price' => $price]);
            $this->lastOrder = $orderResponse;
        } catch (\Exception $ex) {
        }
    }
    
    /**
     * 対象側のを所持してしまった場合成売り
     */
    private function sellSub()
    {
        $subAmount = $this->accountInfo[$this->marketSub]['available'];
        if (bccomp($subAmount, 0, 8) === 0) {
            return;
        }
        try {
            $this->api->iocOrder(['market' => $this->market, 'type' => 'sell', 'amount' => $subAmount, 'price' => $this->marketDepthData[$this->market]['bids'][0][0]]);
        } catch (\Exception $ex) {
        }
    }
    
    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->delay / 1000000)));
        
        // 板
        $this->output->writeln(sprintf('<comment>%s板状況</comment>', $this->market));
        for ($i = 4; $i >= 0; $i--) {
            $this->output->writeln(sprintf('  <info>%.8f - %.4f</info>', $this->marketDepthData[$this->market]['asks'][$i][0], $this->marketDepthData[$this->market]['asks'][$i][1]));
        }
        $this->output->writeln('<info>--------------------</info>');
        $this->output->writeln(sprintf('  <info>%.8f</info>', $this->marketDepthData[$this->market]['last']));   
        $this->output->writeln('<info>--------------------</info>');
        for ($i = 0; $i <= 4; $i++) {
            $this->output->writeln(sprintf('  <info>%.8f - %.4f</info>', $this->marketDepthData[$this->market]['bids'][$i][0], $this->marketDepthData[$this->market]['bids'][$i][1]));
        }
        
        // 現在の注文
        $this->output->writeln(sprintf('<comment>現在の注文状況</comment>'));
        if (!empty($this->lastOrder)) {
            $this->output->writeln(sprintf('  <info>価格: %.8f</info>', $this->lastOrder['price']));
            $this->output->writeln(sprintf('  <info>数量: %.8f</info>', $this->lastOrder['amount']));
            $this->output->writeln(sprintf('  <info>日時: %s</info>', date('Y-m-d H:i:s', $this->lastOrder['create_time'])));
        } else {
            $this->output->writeln('  <info>価格: なし</info>');
            $this->output->writeln('  <info>数量: なし</info>');
            $this->output->writeln('  <info>日時: なし</info>');
        }
        
        // 残高
        $this->outputAccountInfo();
        
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    /**
     * Check order
     */
    private function checkOrder()
    {
        // 注文がなければ何もしない
        if (empty($this->lastOrder)) {
            return;
        }
        $id = $this->lastOrder['id'];
        
        $cancel = false;
        
        // 注文データ取得
        
        try {
            $data = $this->api->getOrderStatus(['market' => $this->market, 'id' => $id]);
        } catch (\Exception $ex) {
            $cancel = true;
        }
        if (!empty($data)) {
            // 一部でも約定している場合
            if ($data['status'] !== 'not_deal') {
                $cancel = true;
            }

            // 指定位置よりもずれている場合
            if (bccomp($data['price'], $this->getOrderPrice(), 8) !== 0) {
                $cancel = true;
            }
        }
        
        // キャンセル
        if ($cancel) {
            try {
                $this->api->cancelOrder(['market' => $this->market, 'id' => $id]);
            } catch (\Exception $ex) {
            }
            $this->lastOrder = null;
        }
        
    }
    
    /**
     * 注文価格を取得
     */
    private function getOrderPrice() {
        $basePrice = $this->marketDepthData[$this->market]['bids'][0][0];
        $price = bcsub($basePrice, bcmul('0.00000001', $this->position, 8), 8);
        // 売り値と同じ場合を考慮
        if (bccomp($this->marketDepthData[$this->market]['asks'][0][0], $price, 8) !== 1) {
            $price = bcsub($this->marketDepthData[$this->market]['asks'][0][0], bcmul(0.00000001, $this->position, 8), 8);
        }
        
        return $price;
    }
    
}