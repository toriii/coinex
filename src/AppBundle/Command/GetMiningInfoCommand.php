<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Ranking\RankingData;

/**
 * Test
 */
class GetMiningInfoCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:mining-info:get')
            ->setDescription('get Mining Info')
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $url = 'https://www.coinex.com/res/mining/info';
        // パス
        $dir = realpath($this->getContainer()->get('kernel')->getRootDir() . '/../web/mining');
        
        $date = date('YmdHi');
        
        
        
        $output->writeln(sprintf('<comment>Mining Info : %s</comment>', date('Y-m-d H:i')));
        
        $json = file_get_contents($url);
        $data = json_decode($json, true);
        
        
        $alreadyMining = $data['data']['already_mining'];
        $lockAmount = $data['data']['lock_amount'];
        
        $output->writeln(sprintf('<info>  Already Mining : %.8f CET</info>', $alreadyMining));
        $output->writeln(sprintf('<info>  Lock Amount : %.8f CET</info>', $lockAmount));

        
        
        // ファイル名
        $fileName = sprintf('%s.json', $date);

        // ファイルパス
        $filePath = sprintf('%s/%s',$dir, $fileName);
        if (file_exists($filePath)) {
            return;
        }
        
        $output->writeln(sprintf('<comment>File : %s</comment>', $fileName));
        file_put_contents($filePath, $json);
    }
    
    
}