<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Input\InputOption;

/**
 * FeeConsumptionCommand
 */
class FeeConsumptionCommand extends BaseBotCommand
{

    /**
     *
     * @var type 
     */
    private $fees = ['CET' => 0];
    
    /**
     * Messages
     * @var type 
     */
    private $messages = [];
    
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:trade:fee-consumption')
            ->setDescription('Fee Consumption')
            ->addArgument('market', InputArgument::REQUIRED, 'ペア')
            ->addArgument('order-amount', InputArgument::REQUIRED, '1回の注文量')
        ;
    }
    
    /**
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        
        // Marketをセット
        $this->setMarket($this->input->getArgument('market'));
    }



    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
       
        do {
            // 残高更新
            $this->updateAccountInfo();
            
            // 板情報取得
            $this->updateMarketDepthData([$this->market]);
            
            // 調整
            
            // 注文
            $this->order();
            
            // 情報出力
            $this->outputData();
            
            // 待機
            usleep($this->delay);
        } while (true);
        
    }
    
    /**
     * Order
     */
    private function order()
    {
        $priceSum = bcadd($this->marketDepthData[$this->market]['asks'][0][0], $this->marketDepthData[$this->market]['bids'][0][0], $this->getMarketMerge());
        $priceAvrage = bcdiv($priceSum, 2, $this->getMarketMerge());
        if (bccomp($priceAvrage, $this->marketDepthData[$this->market]['asks'][0][0], $this->getMarketMerge()) === 0 || bccomp($priceAvrage, $this->marketDepthData[$this->market]['bids'][0][0], $this->getMarketMerge()) === 0){
            $this->messages[] = '板ぴったり....';
        }
        $price = $priceAvrage;
        
        $amount = $this->input->getArgument('order-amount');
        
        // 注文
        try {
            $sellResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'sell', 'amount' => $amount, 'price' => $price]);
        } catch (\Exception $ex) {
        }
        
        try {
            $buyResponse = $this->api->limitOrder(['market' => $this->market, 'type' => 'buy', 'amount' => $amount, 'price' => $price]);
        } catch (\Exception $ex) {
        }
                
                
        // キャンセル
        try {
            if (!empty($sellResponse['id'])) {
                $scor = $this->api->cancelOrder(['market' => $this->market, 'id' => $sellResponse['id']]);
            }
        } catch (\Exception $ex) {
        }
        try {
            if (!empty($buyResponse['id'])) {
                $bcor = $this->api->cancelOrder(['market' => $this->market, 'id' => $buyResponse['id']]);
            }
        } catch (\Exception $ex) {
        }
        // 注文チェック
        try {
            if (!empty($sellResponse['id'])) {
                $sellOrderStatus = $this->api->getOrderStatus(['market' => $this->market, 'id' => $sellResponse['id']]);
                if (!empty($sellOrderStatus)) {
                    if (!isset($this->fees[$sellOrderStatus['fee_asset']])) {
                        $this->fees[$sellOrderStatus['fee_asset']] = 0;
                    }
                    $this->fees[$sellOrderStatus['fee_asset']] = bcadd($this->fees[$sellOrderStatus['fee_asset']], $sellOrderStatus['asset_fee'], 8);
                }
            }
        } catch (\Exception $ex) {
        }
        try {
            if (!empty($buyResponse['id'])) {
                $buyOrderStatus = $this->api->getOrderStatus(['market' => $this->market, 'id' => $buyResponse['id']]);
                if (!isset($this->fees[$buyOrderStatus['fee_asset']])) {
                    $this->fees[$buyOrderStatus['fee_asset']] = 0;
                }
                $this->fees[$buyOrderStatus['fee_asset']] = bcadd($this->fees[$buyOrderStatus['fee_asset']], $buyOrderStatus['asset_fee'], 8);
            }
        } catch (\Exception $ex) {
        }
        
        
    }
    
    /**
     * 注文金額を取得
     */
    private function getOrderPrice()
    {
        $difference = bcsub($this->marketDepthData[$this->market]['asks'][0][0], $this->marketDepthData[$this->market]['bids'][0][0], $this->getMarketMerge());
        $price = bcadd($this->marketDepthData[$this->market]['bids'][0][0], bcdiv($difference, 2, $this->getMarketMerge()), $this->getMarketMerge());
        
        return $price;
    }
    
    
    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->getDelayM())));
                
        // 板情報
        $this->outputMarketDepthData($this->market, 5);
        
        // 残高
        $outputCoin = [$this->marketMain, $this->marketSub];
        if (!in_array('CET', $outputCoin)) {
            $outputCoin[] = 'CET';
        }
        $this->outputAccountInfo($outputCoin);
        
        $this->output->writeln(sprintf('<comment>消費手数料</comment>'));
        foreach ($this->fees as $coin => $fee) {
            $this->output->writeln(sprintf('  <info>%s : %.8f</info>', $coin, $fee));
        }
        
        $this->output->writeln(sprintf('<comment>その他</comment>'));
        foreach ($this->messages as $message) {
            $this->output->writeln(sprintf('  <info>%s</info>', $message));
        }
        if (empty($this->messages)) {
            $this->output->writeln(sprintf('  <info>なし</info>'));
        }
        $this->messages = [];
        
        $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■', date('s'))));
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    
}