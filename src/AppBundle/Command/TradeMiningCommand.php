<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Test
 */
class TradeMiningCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:trade:mining')
            ->setDescription('Trade mining')
            ->addArgument('market', InputArgument::OPTIONAL, 'Market')
            ->addArgument('useCetAsFees', InputArgument::OPTIONAL, 'Use CET as Fees')
            ->addArgument('delay', InputArgument::OPTIONAL, 'sleep')
                
        ;
    }

    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
       
        $api = $this->getContainer()->get('coinex_api');
        $helper = $this->getHelper('question');
        $updateTime = time();
        
        
        
        
        $delay = (float)$input->getArgument('delay');
        $botDelay = ($delay >= 0.5 && $delay <=5) ? ($delay * 1000000) : 1000000;
        $delaym = bcdiv($botDelay, 1000000, 2);
        
        $miningLimit = 0.95;
        
        
        $useCetAsFees = (boolean)$input->getArgument('useCetAsFees');

        //　ペアを取得
        $marketList = $api->getMarketList();
        $market = $input->getArgument('market');
        if (empty($market) || !in_array($market, $marketList)) {
            //　残高を表示
            $output->writeln(sprintf('<comment>Balance Info</comment>'));
            foreach ($api->getAccountInfo() as $key => $val) {
                $output->writeln(sprintf('<info>%6s : %.8f</info>', $key, $val['available']));
            }
            
            // ペアを選ぶ
            $marketQuestion = new ChoiceQuestion('Market?', $marketList);
            $market = $helper->ask($input, $output, $marketQuestion);
        }
        foreach (['USDT', 'BCH', 'BTC', 'ETH', 'CET'] as $val) {
            if (strpos($market, $val) !== false) {
                $main = $val;
                $target = str_replace($val, '', $market);
                break;
            }
        }

        if ($main != 'CET') {
            $cetBchData = $api->getMarketDepth(['market' => 'CET' . $main, 'merge' => 0.00000001, 'limit' => 5]);
        }
        while (true) {
            // マイニング状況の確認
            if (time() >= $updateTime) {
                $miningDifficulty = $api->getMiningDifficulty();
                $percentageToUpperLimit = $miningDifficulty['difficulty'] == 0 ? 0 : bcdiv($miningDifficulty['prediction'], $miningDifficulty['difficulty'], 8); 
                $updateTime = ($updateTime == $miningDifficulty['update_time'] + 30) ? (time() + 10)  : $miningDifficulty['update_time'] + 30;
            }
            // 残高
            $accountInfo = $api->getAccountInfo();
                
            // 板を取得
            $depthData = $api->getMarketDepth(['market' => $market, 'merge' => 0.00000001, 'limit' => 5]);
            
            $tradeDiff = bcsub($depthData['asks'][0][0], $depthData['bids'][0][0], 8);
                        
            $price = bcadd($depthData['bids'][0][0], bcdiv($tradeDiff, 2, 8), 8);
            $remaining = bcsub($miningDifficulty['difficulty'], $miningDifficulty['prediction'], 8);  // マイニング上限までの残り
            $remainingTime = (60 - date('i')) * 60 - date('s'); // 残り時間（秒）
            $amountOfOneSecond = $remaining / $remainingTime; // 1秒で必要な手数料CET
            if ($main != 'CET') {
                $amountOfOneSecondBch = bcmul($amountOfOneSecond, $cetBchData['last'], 8); // 1秒で必要な手数料BCH
            } else {
                $amountOfOneSecondBch = $amountOfOneSecond;
            }
            // 補正値
            if ($percentageToUpperLimit < 0.5 && date('i') < 20) {
                $correctionValue = 5;
            } elseif ($percentageToUpperLimit < 0.7 && date('i') < 30) {
                $correctionValue = 4;
            } elseif ($percentageToUpperLimit < 0.8 && date('i') < 45) {
                $correctionValue = 2;
            } elseif ($percentageToUpperLimit < 0.85 && date('i') < 50) {
                $correctionValue = 1.5;
            } elseif ($percentageToUpperLimit < 0.9 && date('i') < 55) {
                $correctionValue = 1;
            } else {
                $correctionValue = 0.2;
            }
            
            $baseAmount = bcmul(bcdiv($amountOfOneSecondBch, $price, 8), 500, 8);
            $amount = bcmul($baseAmount, $correctionValue, 4); // 1秒に必要な取引量
            
            if ($delaym < 1) {
                $delaym = $delaym * 1.5;
            }
            $amount = bcmul($amount, $delaym, 8);
            
            if ($useCetAsFees) {
                $amount = bcmul($amount, 2, 8);
            }
            
            // 残高による調整
            if (bccomp($accountInfo[$target]['available'], $amount, 8) === -1) {
                $amount = bcmul($accountInfo[$target]['available'], 0.8, 8);
            }
            $priceSum = bcmul($price, $amount, 8);
            if (bccomp($accountInfo[$main]['available'], $priceSum, 8) === -1) {
                $amount = bcmul(bcdiv($accountInfo[$main]['available'], $price, 8), 0.8, 8);
            }
            
            // 取引
            $tradeMessages = [];
            if ($percentageToUpperLimit >= $miningLimit) {
                $tradeMessages[] = sprintf('%.2f%%以上のため%sまで停止', $miningLimit * 100, date('H:00', strtotime('+1 hour')));
            } else if (bccomp($price, $depthData['bids'][0][0], 8) === 0 || bccomp($price, $depthData['asks'][0][0], 8) === 0) {
                $tradeMessages[] = '取引が安定するのを待っています。';
            } elseif ($miningDifficulty['difficulty'] == 0) {
                $tradeMessages[] = 'マイ採掘難易度が0です。（09:00過ぎの場合は反映待ち？）';
            }else {
                
                try {
                    $sellResponse = $api->limitOrder(['market' => $market, 'type' => 'sell', 'amount' => $amount, 'price' => $price]);
                } catch (\Exception $ex) {
                }
                try {
                    $buyResponse = $api->limitOrder(['market' => $market, 'type' => 'buy', 'amount' => $amount, 'price' => $price]);
                } catch (\Exception $ex) {
                }
                
                
                // キャンセル
                try {
                    if (true) {
                        $scor = $api->cancelOrder(['market' => $market, 'id' => $sellResponse['id']]);
                    }
                } catch (\Exception $ex) {
                }
                try {
                    $bcor = $api->cancelOrder(['market' => $market, 'id' => $buyResponse['id']]);
                } catch (\Exception $ex) {
                }
                
                $tradeMessages[] = sprintf('価格 %.8f %s - 量 %f %s', $price, $main, $amount, $target);
                
                // 残高
                $accountInfo = $api->getAccountInfo();
                
                
            }
            
            
            // 調整
            $sum = bcadd($accountInfo[$main]['available'], bcmul($accountInfo[$target]['available'], $price , 8), 8);
            $mainRate = bcdiv($accountInfo[$main]['available'], $sum, 8) * 100;
            $targetRate = bcdiv(bcmul($accountInfo[$target]['available'], $price , 8), $sum, 8) * 100;
                
            $adjustmentType = ($mainRate > $targetRate) ? 'buy' : 'sell';
            $adjustmentDiff = abs(50 - $targetRate) * 0.01;
            $targetAmountSum = bcadd(bcdiv($accountInfo[$main]['available'], $price, 8), $accountInfo[$target]['available'], 8); // 対象側換算の総量
            
            if ($adjustmentDiff > 0.03 && ($percentageToUpperLimit < $miningLimit)) {
//                $adjustmentAmount = ($mainRate > $targetRate) ? sprintf('%.8f', $sum * 0.01) : sprintf('%.8f', $targetAmountSum * 0.01);
                $adjustmentAmount = sprintf('%.8f', $targetAmountSum * 0.01);
                $adjustmentPrice = ($mainRate > $targetRate) ? $depthData['asks'][0][0] : $depthData['bids'][0][0];
                $api->iocOrder(['market' => $market, 'type' => $adjustmentType, 'amount' => $adjustmentAmount, 'price' => $adjustmentPrice]);
            }
            

            $output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($botDelay / 1000000)));
            
            // マイニング状況の表示
            $output->writeln(sprintf('<comment>マイニング状況</comment>'));
            $output->writeln(sprintf('  <info>マイ採掘難易度 　　： %.2f CET/毎時</info>', $miningDifficulty['difficulty']));
            $output->writeln(sprintf('  <info>１時間のマイニング ： %.8f CET</info>', $miningDifficulty['prediction']));
            $output->writeln(sprintf('  <info>最終更新時間　　　 ： %s (%.2f%%)</info>', date('Y-m-d H:i:s', $miningDifficulty['update_time']), (date('i', $miningDifficulty['update_time']) / 60 * 100) ));
            $output->writeln(sprintf('  <info>上限までの割合　　 ： %.2f%%</info>', $percentageToUpperLimit * 100));
            
            
            //　板情報を表示
            $output->writeln(sprintf('<comment>%s/%s板状況</comment>', $target, $main));
            $output->writeln(sprintf('  <info>買い板: %.8f</info>', $depthData['bids'][0][0]));
            $output->writeln(sprintf('  <info>売り板: %.8f</info>', $depthData['asks'][0][0]));
            $output->writeln(sprintf('  <info>最終　: %.8f</info>', $depthData['last']));     
            
            
            // 取引情報を表示
            $output->writeln(sprintf('<comment>取引状況</comment>'));
            foreach ($tradeMessages as $tradeMessage) {
                $output->writeln(sprintf('  <info>%s</info>', $tradeMessage));
            }
            
            // 残高
            $output->writeln(sprintf('<comment>残高状況</comment>'));
            $output->writeln(sprintf('  <info>%s : %.8f (%.2f%%)</info>', $main, $accountInfo[$main]['available'], $mainRate));
            $output->writeln(sprintf('  <info>%s : %.8f (%.2f%%)</info>', $target, $accountInfo[$target]['available'], $targetRate));
            
            $output->writeln(sprintf('<comment>%s</comment>', str_repeat('■', date('s'))));
            
            $output->writeln('<info>================================================================================</info>');
            
            
            usleep($botDelay);
        }
        
    }
    
    
}