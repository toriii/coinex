<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Input\InputOption;

/**
 * AskBidMineCetCommand
 */
class AskBidMineCet3Command extends BaseBotCommand
{

    /**
     * 買い注文を出すか
     * @var boolean 
     */
    private $buy;

    /**
     * 売り注文を出すか
     * @var boolean 
     */
    private $sell;
    
    /**
     * 注文のUSD換算量
     * @var intger
     */
    private $usd;
    
    /**
     * 最大買い注文量
     * Max order num
     * @var number
     */
    private $maxBuyOrderNum;
    
    /**
     * 最大売り注文量
     * Max order num
     * @var number
     */
    private $maxSellOrderNum;
    
    /**
     * 狙う毎分の順位
     * @var tnumber
     */
    private $targetOrder;
    
    /**
     * 残高調整の設定
     * @var type 
     */
    private $balanceAdjustmentSetting;
    
    /**
     *現在の注文
     * @var type 
     */
    private $orders = [];
    
    /**
     *
     * @var type 
     */
    private $outputData = [
        'orderNum' => ['buy' => 0, 'sell' => 0],
        'orderPrice' => ['buy' => 0, 'sell' => 0],
        'scheduledRank' => ['buy' => 0, 'sell' => 0],
        'cancelNum' => ['buy' => 0, 'sell' => 0],
    ];
    
    /**
     * Configure
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('coinex:trade:ask-bid-mine-cet-3')
            ->setDescription('Ask, Bid & Mine CET ver.3')
            ->addArgument('market', InputArgument::REQUIRED, '指し値注文を出すペア')
            ->addOption('buy', null, InputOption::VALUE_REQUIRED, '買いで指し値を出す？（true or false）', 'true')
            ->addOption('sell', null, InputOption::VALUE_REQUIRED, '売りで指し値を出す？（true or false）', 'true')
            ->addOption('usd', null, InputOption::VALUE_REQUIRED, '1注文あたりのドル関連量？（ルールでは最低100USD必要）', 110)
            ->addOption('max-buy-order-num', null, InputOption::VALUE_REQUIRED, '最大買い注文数（指定なしで無制限）', NULL)
            ->addOption('max-sell-order-num', null, InputOption::VALUE_REQUIRED, '最大買い注文数（指定なしで無制限）', NULL)
            ->addOption('buy-target-order', null, InputOption::VALUE_REQUIRED, '買いで狙う毎分の順位（順位×100USDの後ろ狙い）', 1)
            ->addOption('sell-target-order', null, InputOption::VALUE_REQUIRED, '売り狙う毎分の順位（順位×100USDの後ろ狙い）', 1)
            ->addOption('balance-adjustment', null, InputOption::VALUE_REQUIRED, '片方注文のみの約定後調整（0:しない,1:即戻す）', 1)
            ->addOption('cancel-delay', null, InputOption::VALUE_REQUIRED, 'キャンセル間隔', 1)
            ->addOption('only-output', null, InputOption::VALUE_REQUIRED, '出力のみ', false)
        ;
    }
    
    /**
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        
        // Marketをセット
        $this->setMarket($this->input->getArgument('market'));
        
        // 注文タイプを設定
        $this->setOrderType();
        
        $this->maxBuyOrderNum = filter_var($this->input->getOption('max-buy-order-num'), FILTER_VALIDATE_INT);
        $this->maxSellOrderNum = filter_var($this->input->getOption('max-sell-order-num'), FILTER_VALIDATE_INT);
        $this->usd = filter_var($this->input->getOption('usd'), FILTER_VALIDATE_INT);
        $this->balanceAdjustmentSetting = filter_var($this->input->getOption('balance-adjustment'), FILTER_VALIDATE_INT);
        
        
        $buyTargetOrder = filter_var($this->input->getOption('buy-target-order'), FILTER_VALIDATE_INT);
        $sellTargetOrder = filter_var($this->input->getOption('sell-target-order'), FILTER_VALIDATE_INT);
        $this->targetOrder = ['buy' => $buyTargetOrder, 'sell' => $sellTargetOrder];
        
        $this->cancelDelay = filter_var($this->input->getOption('cancel-delay'), FILTER_VALIDATE_INT);
        
        
        $this->onlyOutput = filter_var($this->input->getOption('only-output'), FILTER_VALIDATE_BOOLEAN);
    }



    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        $count = 0;
        do {
            $count++;
            
            $this->outputData = [
                'orderNum' => ['buy' => 0, 'sell' => 0],
                'orderPrice' => ['buy' => 0, 'sell' => 0],
                'scheduledRank' => ['buy' => 0, 'sell' => 0],
                'cancelNum' => ['buy' => 0, 'sell' => 0],
            ];
            
            
            // 残高取得
            $this->updateAccountInfo();
            
            // 板情報取得
            $this->updateMarketDepthData([$this->market], 20);
            
            // 注文状況更新
            $this->getOrders();
            
            // 注文が片方の場合に逆側を処分
            $this->balanceAdjustment();
            
            // 注文をチェック
            if ($count == $this->cancelDelay) {
                $this->checkOrders();
                $count = 0;
            }
            
            // 注文
            if ($this->buy) {
                $this->order('buy');
            }
            if ($this->sell) {
                $this->order('sell');
            }
            
            // 情報出力
            $this->outputData();
            
            // 待機
            usleep($this->delay);
        } while (true);
        
    }
    
    /**
     * 注文タイプの設定
     * Set order type
     * 
     */
    private function setOrderType()
    {
        $this->buy = filter_var($this->input->getOption('buy'), FILTER_VALIDATE_BOOLEAN);
        $this->sell = filter_var($this->input->getOption('sell'), FILTER_VALIDATE_BOOLEAN);
        
        if (!$this->buy && !$this->sell) {
            $this->output->writeln('<error>買い・売りどちらの注文も無効になっています。</error>');
            exit;
        }
    }
    
    
    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->getDelayM())));
        
        // 設定
        $this->output->writeln(sprintf('<comment>設定</comment>'));
        $this->output->writeln(sprintf('  <info>注文タイプ</info>'));
        $this->output->writeln(sprintf(' 　　 <info>買い： %s</info>', $this->input->getOption('buy')));
        $this->output->writeln(sprintf(' 　　 <info>売り： %s</info>', $this->input->getOption('sell')));
        $this->output->writeln(sprintf('  <info>狙う順位</info>'));
        $this->output->writeln(sprintf(' 　　 <info>買い： %d位(%d USD の後ろ)</info>', $this->targetOrder['buy'], $this->targetOrder['buy'] * 100));
        $this->output->writeln(sprintf(' 　　 <info>売り： %d位(%d USD の後ろ)</info>', $this->targetOrder['sell'], $this->targetOrder['sell'] * 100));
        
        
        
        
        // 板情報
        $this->outputMarketDepthData($this->market, 10, true);
        
        // 残高
        $this->outputAccountInfo([$this->marketMain, $this->marketSub], true);
        
        // その他
        $this->output->writeln(sprintf('<comment>その他情報</comment>'));
        $this->output->writeln(sprintf('  <info>注文した数</info>'));
        $this->output->writeln(sprintf('    <info>買い：%d</info>', $this->outputData['orderNum']['buy']));
        $this->output->writeln(sprintf('    <info>売り：%d</info>', $this->outputData['orderNum']['sell']));
        
        $this->output->writeln(sprintf('  <info>キャンセルした数</info>'));
        $this->output->writeln(sprintf('    <info>買い：%d</info>', $this->outputData['cancelNum']['buy']));
        $this->output->writeln(sprintf('    <info>売り：%d</info>', $this->outputData['cancelNum']['sell']));
        
        $totalOrderNum = ['buy' => 0, 'sell' => 0];
        foreach ($this->orders as $order) {
            $totalOrderNum[$order['type']]++;
        }
        $this->output->writeln(sprintf('  <info>現在の注文数</info>'));
        $this->output->writeln(sprintf('    <info>買い：%d</info>', $totalOrderNum['buy']));
        $this->output->writeln(sprintf('    <info>売り：%d</info>', $totalOrderNum['sell']));
        $this->output->writeln(sprintf('  <info>注文価格計算</info>'));
        $this->output->writeln(sprintf('    <info>買い：%.8f (%d位以内予定)</info>', $this->outputData['orderPrice']['buy'], $this->outputData['scheduledRank']['buy']));
        $this->output->writeln(sprintf('    <info>売り：%.8f (%d位以内予定)</info>', $this->outputData['orderPrice']['sell'], $this->outputData['scheduledRank']['sell']));
        
        
        $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■', date('s'))));
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    /**
     * Update market depth data
     */
    protected function updateMarketDepthData(array $markets = [], $limit = 5)
    {        
        
        // 基軸がドル以外の場合
        if ($this->marketMain !== $this->usdCoin) {
            $markets[] = sprintf('%s%s', $this->marketMain, $this->usdCoin);
        }
        
        parent::updateMarketDepthData($markets, $limit);
    }
    
    /**
     * 残高を調整
     */
    private function balanceAdjustment()
    {
        if (!$this->balanceAdjustmentSetting || ($this->buy && $this->sell) || $this->onlyOutput) {
            return;
        }
        
        $mainBalance = sprintf('%.8f', $this->accountInfo[$this->marketMain]['available']);
        $subBalance = sprintf('%.8f', $this->accountInfo[$this->marketSub]['available']);
        if ($this->buy && !$this->sell) { // 買いのみ
            $orderType = 'sell';
            $price = sprintf('%.8f', $this->marketDepthData[$this->market]['bids'][0][0]);
            $baseAmount = $subBalance;
            // 最大注文量がある場合
            if ($this->maxBuyOrderNum) {
                $buyOrderCount = 0;
                foreach ($this->orders as $order) {
                    if ($order['type'] === 'buy') {
                        $buyOrderCount++;
                    }
                }
                $remaining = $this->maxBuyOrderNum - $buyOrderCount; // 残りの可能注文数
                $orderPrice = $this->getOrderPrice('sell'); // 注文する場合の金額
                $orderAmount = $this->getOrderAmount($orderPrice); // 注文する場合の量
                $requiredAmount = bcmul($orderAmount, $remaining * 1.05, 8); // 上限までに必要な残高
                $mainToSubAmount = bcdiv($mainBalance, $orderPrice, 8 );
                $shortageAmount = bcsub($requiredAmount, $mainToSubAmount, 8);
                
                if (bccomp($shortageAmount, 0, 8) !== 1) { // 不足がなければ調整しない
                    $amount = 0;
                } elseif (bccomp($baseAmount, $shortageAmount, 8) === 1) { // 超えている場合
                    $amount = $shortageAmount; // 不足分だけにする
                } else {
                    $amount = $baseAmount;
                }                
            } else {
                $amount = $baseAmount;
            }
        } elseif ($this->sell && !$this->buy) { // 売りのみ
            $orderType = 'buy';
            $price = sprintf('%.8f', $this->marketDepthData[$this->market]['asks'][0][0]);
            $baseAmount = bcdiv($mainBalance, $price, 8);
            // 最大注文量がある場合
            if ($this->maxSellOrderNum) {
                $sellOrderCount = 0;
                foreach ($this->orders as $order) {
                    if ($order['type'] === 'sell') {
                        $sellOrderCount++;
                    }
                }
                $remaining = $this->maxSellOrderNum - $sellOrderCount; // 残りの可能注文数
                $orderPrice = $this->getOrderPrice('sell'); // 注文する場合の金額
                $orderAmount = $this->getOrderAmount($orderPrice); // 注文する場合の量
                $requiredAmount = bcmul($orderAmount, $remaining * 1.05, 8); // 上限までに必要な残高
                $shortageAmount = bcsub($requiredAmount, $subBalance, 8);
                if (bccomp($shortageAmount, 0, 8) !== 1) { // 不足がなければ調整しない
                    $amount = 0;
                } elseif (bccomp($baseAmount, $shortageAmount, 8) === 1) { // 超えている場合
                    $amount = $shortageAmount; // 不足分だけにする
                } else {
                    $amount = $baseAmount;
                }
            } else {
                $amount = $baseAmount;
            }
        }
        
        
        // 一定未満の場合は行わない
        $usdAmount = $this->subToUsd($amount);
        if (bccomp($usdAmount, 1, 8) !== 1) {
            return;
        }
        
        
        try {
            $this->api->iocOrder(['market' => $this->market, 'type' => $orderType, 'amount' => $amount, 'price' => $price]);
        } catch (\Exception $ex) {
        }
        
    }
        
    /**
     * Order
     */
    private function order($type)
    {
        $price = $this->getOrderPrice($type);
        $amount = $this->getOrderAmount($price);
        $orderNum = $this->getOrderNum($type, $price);
        
        $this->outputData['orderNum'][$type] = 0;
        for ($i = 0; $i < $orderNum; $i++) {
            // 注文APIにリクエスト
            try {
                if (!$this->onlyOutput) {
                    $orderResponse = $this->api->limitOrder(['market' => $this->market, 'type' => $type, 'amount' => $amount, 'price' => $price]);
                    $this->orders[$orderResponse['id']] = $orderResponse;
                }
            } catch (\Exception $ex) {
            }
            $this->outputData['orderNum'][$type]++;
            usleep(500000);
        }
    }
    
    /**
     * Get order price
     */
    private function getOrderPrice($orderType)
    {
        $priceType = $this->orderTypeToPriceType($orderType);
        
        $usdSum = 0;
        $beforeUsdAmount = $this->targetOrder[$orderType] * 100;
        
        
        // 自分の注文分を除く
        $myOrderList = [];
        foreach ($this->orders as $order) {
            $myOrderPrice = sprintf('%.8f', $order['price']);
            $myOrderAmount = sprintf('%.8f', $order['amount']);
            if (!isset($myOrderList[$myOrderPrice])) {
                $myOrderList[$myOrderPrice] = 0;
            }
            $myOrderList[$myOrderPrice] = bcadd($myOrderList[$myOrderPrice], $myOrderAmount, 8);
        }
        
        $originDepthData = $this->marketDepthData[$this->market][$priceType];
        $depthData = [];
        foreach ($originDepthData as $key => $val) {
            $depthPrice = sprintf('%.8f', $val[0]);
            $depthAmount = sprintf('%.8f', $val[1]);
            if (!empty($myOrderList[$depthPrice])) {
                $depthAmount = bcsub($depthAmount, $myOrderList[$depthPrice], 8);
            }
            if (bccomp($depthAmount, 0, 8) === 1) {
                $depthData[] = [$depthPrice, $depthAmount];
            }
        }
        
        $over100Count = 0;
        foreach ($depthData as $key => $val) {
            $orderUsdAmount = $this->subToUsd($val[1], $val[0]);
            if ($orderUsdAmount > 100) {
                $over100Count += floor($orderUsdAmount / 100);
            }
            $usdSum += $orderUsdAmount;
            $basePrice = sprintf('%.8f', $val[0]); // 基準となる価格
            
//            printf("%.8f - %.5f USD - %d \n", $val[0], $orderUsdAmount, $over100Count);
            if ($usdSum > $beforeUsdAmount) {
                break;
            }
        }
        // 最低単位分動かせるかチェック
        if ($orderType === 'buy') {
            $afterPrice = bcsub($basePrice, $this->getMerge(), 8);
        } else {
            $afterPrice = bcadd($basePrice, $this->getMerge(), 8);
        }
        if (isset($depthData[$key + 1]) && bccomp(sprintf('%.8f', $depthData[$key + 1][0]), $afterPrice, 8) !== 0) {
            $basePrice = $afterPrice;
        }
        
        
        // 5%をチェック
        $limitPrice = $this->getLimitPrice($orderType);
        
        if ((bccomp($limitPrice, $basePrice, 8) === 1 && $orderType === 'buy') || (bccomp($basePrice, $limitPrice, 8) === 1 && $orderType === 'sell')) {
            $basePrice = $limitPrice;
        }
        $orderPrice = $basePrice;
        
        $this->outputData['orderPrice'][$orderType] = $orderPrice;
        $this->outputData['scheduledRank'][$orderType] = $over100Count + 1;
        
        return $orderPrice;
    }
    
    /**
     * 1個の注文量を算出
     * @param type $price
     * @return $amount
     */
    private function getOrderAmount($price)
    {
        if ($this->marketMain === $this->usdCoin) {
            $amount = bcdiv($this->usd, $price, 8);
        } else {            
            $orderPriceSum = bcdiv($this->usd, $this->marketDepthData[$this->marketMain . $this->usdCoin]['last'], 8);
            $amount = bcdiv($orderPriceSum, $price, 8);
        }
        
        return $amount;
    }
    
    /**
     * 注文数を取得
     * Get order price
     */
    private function getOrderNum($type, $price)
    {
        $count = 0;
        foreach ($this->orders as $order) {
            if ($order['type'] === $type) {
                $count++;
            }
        }
        if ($type === 'buy') {
            // 最大の買い注文量
            $amountToBuy = bcdiv($this->accountInfo[$this->marketMain]['available'], $price, 8);
            $usdtToBuy = $this->subToUsd($amountToBuy, $price);
            $num = (int)bcdiv($usdtToBuy, $this->usd, 8);

            // 設定なしならそのまま
            if (!$this->maxBuyOrderNum) {
                
            } elseif ($count >= $this->maxBuyOrderNum) { // 既に上限を超えていたら
                $num = 0;
            } elseif (($num + $count) > $this->maxBuyOrderNum) { // 合計が超えるなら
                $num = $this->maxBuyOrderNum - $count;
            }
        } else {
            // 最大の売り量
            $amountToSell = $this->accountInfo[$this->marketSub]['available'];
            $usdtToSell = $this->subToUsd($amountToSell, $price);
            $num = (int)bcdiv($usdtToSell, $this->usd, 8);

            // 設定なしならそのまま
            if (!$this->maxSellOrderNum) {
            } elseif ($count >= $this->maxSellOrderNum) { // 既に上限を超えていたら
                $num = 0;
            } elseif (($num + $count) > $this->maxSellOrderNum) { // 合計が超えるなら
                $num = $this->maxSellOrderNum - $count;
            } else {
            }
        }
                
        return $num;
    }
    
    /**
     * 注文をチェック
     */
    private function checkOrders()
    {
        // 注文データを取得
        $this->outputData['cancelNum'] = ['buy' => 0, 'sell' => 0];
        // 注文がなければ何もしない
        if (empty($this->orders)) {
            return;
        }
        $this->tmpOrders = $this->orders;
        
        foreach ($this->orders as $key => $order) {
            $cancel = false;
            
            // 一部でも約定している場合
            if ($order['status'] !== 'not_deal') {
                $cancel = true;
            }
            
            /*
            // 指定ドル未満になっている場合
            $orderUsdAmount = $this->subToUsd($order['amount'], $order['price']);
            if (bccomp($this->usd, $orderUsdAmount, 8) === 1) {
                $cancel = true;
            }
            */
            
            // 変動により適切な価格でない場合
            $isCancelPrice = $this->isCancelPrice($order);
            if ($isCancelPrice) {
                $cancel = true;
            }
            
            // キャンセル
            if ($cancel && !$this->onlyOutput) {
                try {
                    $this->api->cancelOrder(['market' => $this->market, 'id' => $order['id']]);
                } catch (\Exception $ex) {
                }
                unset($this->tmpOrders[$key]);
                $this->outputData['cancelNum'][$order['type']]++;
                usleep(200000);
            }
            
        }
        
        $this->orders = $this->tmpOrders;
    }
    
    /**
     * 現在の注文を取得
     */
    private function getOrders() {
        $data = $this->api->getUnexecutedOrderList(['market' => $this->market, 'limit' => 100, 'page' => 1]);
        $this->orders = $data['data'];
    }
    
    /**
     * キャンセルする価格かをチェック
     */
    private function isCancelPrice(array $order)
    {
        $orderPrice = $order['price'];
        $type = $order['type'];
        
        // 5%幅を越えていないか
        $limitPrice = $this->getLimitPrice($type);
        if ((bccomp($limitPrice, $orderPrice) === 1 && $type === 'buy') || (bccomp($limitPrice, $orderPrice) === 1 && $type === 'sell')) {
            return true;
        }

        // 理想価格でなければ
        $bestPrice = $this->getOrderPrice($type);
        
        if (bccomp($orderPrice, $bestPrice, 8) !== 0) {
            return true;
        }
        
    }
    
    
}