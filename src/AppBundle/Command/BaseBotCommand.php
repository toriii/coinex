<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Input\InputOption;

/**
 * Test
 */
class BaseBotCommand extends ContainerAwareCommand
{
    /**
     * Input
     * @var InputInterface 
     */
    protected $input;
    
    /**
     * Output
     * @var OutputInterface 
     */
    protected $output;
    
    /**
     *
     * @var Api
     */
    protected $api;
    
    /**
     * Account info
     * @var array
     */
    protected $accountInfo = [];

    /**
     *MArket depth data
     * @var array
     */
    protected $marketDepthData = [];
    
    /**
     * 取引対象のペア
     * @var string
     */
    protected $market;
    
    /**
     * 取引対象のペア（基軸）
     * @var string
     */
    protected $marketMain;
    
    /**
     * 取引対象のペア（購入側）
     * @var string
     */
    protected $marketSub;
    
    /**
     * Delay
     */
    protected $delay = 1000000;

    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:base')
            ->addOption('usd-coin', null, InputOption::VALUE_REQUIRED, 'USD計算用の通貨', 'USDT')
        ;
    }
    
    /**
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        $this->input = $input;
        $this->output = $output;
        $this->api = $this->getContainer()->get('coinex_api');
        
        $this->usdCoin = $this->input->getOption('usd-coin');
        if (!in_array($this->usdCoin, ['USDT', 'USDC'])) {
            $this->output->writeln(sprintf('  <error>「%s」は指定できません</error>', $this->usdCoin));  
            exit;
        }
    }
    
    
    /**
     * 残高を更新
     * Update account info
     */
    protected function updateAccountInfo()
    {
        $this->accountInfo = $this->api->getAccountInfo();
    }
    
    /**
     * 板情報取得
     * Update Market Depth Data
     */
    protected function updateMarketDepthData(array $markets = [], $limit = 5)
    {
        foreach ($markets as $market) {
            $this->marketDepthData[$market] = $this->api->getMarketDepth(['market' => $market, 'merge' => $this->getMerge(), 'limit' => $limit]);
        }
    }
    
    
    /**
     * 残高を出力
     * Output account info
     */
    protected function outputAccountInfo(array $coins = [], $usd = false)
    {        
        $this->output->writeln(sprintf('<comment>残高状況</comment>'));
        foreach ($coins as $coin) {
            if (isset($this->accountInfo[$coin])) {
                $amount = $this->accountInfo[$coin]['available'];
                if ($usd && in_array($coin, [$this->marketMain, $this->marketSub])) {
                    $usdAmount = ($coin === $this->marketMain) ? $this->mainToUsd($amount) : $this->subToUsd($amount);
                    $this->output->writeln(sprintf('  <info>%s : %.8f (%.2f USD)</info>', $coin, $amount, $usdAmount));
                } else {
                    $this->output->writeln(sprintf('  <info>%s : %.8f</info>', $coin, $amount));
                }
            }
        }
    }
    
    
    /**
     * 板情報を出力
     * Output marketDepthData
     */
    protected function outputMarketDepthData($market, $limit = 5, $usd = false)
    {        
        $this->output->writeln(sprintf('<comment>板状況(%s)</comment>', $market));
        if (empty($this->marketDepthData[$market])) {
            $this->output->writeln('  <error>データがありません。</error>');  
            return;
        }
        for ($i = $limit - 1; $i >= 0; $i--) {
            if (isset($this->marketDepthData[$market]['asks'][$i])) {
                $price = $this->marketDepthData[$market]['asks'][$i][0];
                $amount = $this->marketDepthData[$market]['asks'][$i][1];
                if ($usd) {
                    $usdAmount = $this->subToUsd($amount, $price);
                    $this->output->writeln(sprintf('  <info>%.8f - %.4f (%.2f %s)</info>', $price, $amount, $usdAmount, $this->usdCoin));
                } else {
                    $this->output->writeln(sprintf('  <info>%.8f - %.4f</info>', $price, $amount));
                }
            }
        }
        $this->output->writeln('<info>--------------------</info>');
        $this->output->writeln(sprintf('  <info>%.8f</info>', $this->marketDepthData[$market]['last']));   
        $this->output->writeln('<info>--------------------</info>');
        for ($i = 0; $i < $limit; $i++) {
            if (isset($this->marketDepthData[$market]['bids'][$i])) {
                $price = $this->marketDepthData[$market]['bids'][$i][0];
                $amount = $this->marketDepthData[$market]['bids'][$i][1];
                if ($usd) {
                    $usdAmount = $this->subToUsd($amount, $price);
                    $this->output->writeln(sprintf('  <info>%.8f - %.4f (%.2f %s)</info>', $price, $amount, $usdAmount, $this->usdCoin));
                } else {
                    $this->output->writeln(sprintf('  <info>%.8f - %.4f</info>', $price, $amount));
                }
            }
        }
    }
    
    /**
     * Set market
     * @param type $market
     */
    protected function setMarket($market)
    {
        $mainList = ['USDT', 'USDC','BCH', 'BTC', 'ETH', 'CET'];
        
        // マーケット一覧を取得
        $marketList = $this->api->getMarketList();
        
        // 存在するペアかチェック
        if (!in_array($market, $marketList)) {
            $this->output->writeln(sprintf('<error>%s は指定できないペアです。</error>', $market));
            exit;
        }
        $this->market = $market;
        foreach ($mainList as $val) {
            if (strpos($market, $val) !== false) {
                $this->marketMain = $val;
                break;
            }
        }
        $this->marketSub = str_replace($this->marketMain, '', $this->market);
    }
    
    /**
     * 秒で実行間隔を取得
     */
    protected function getDelayM()
    {
        return bcdiv($this->delay, 1000000, 2);
    }
    
    /**
     * 基軸のドル換算を取得
     * @param type $amount
     */
    protected function mainToUsd($amount)
    {
        if ($this->marketMain === $this->usdCoin) {
            $usdAmount = $amount;
        } else {
            $mainUsdtPrice = sprintf('%.8f', $this->marketDepthData[$this->marketMain . $this->usdCoin]['last']);
            $usdAmount = bcmul($amount, $mainUsdtPrice, 8);
        }
        
        return $usdAmount;
    }
    
    /**
     * 
     * @param type $amount
     * @param type $subPrice
     */
    protected function subToMain($amount, $subPrice = null)
    {
        
        $subPrice = ($subPrice === null) ? $this->marketDepthData[$this->market]['last'] : $subPrice;
        $mainAmount = bcmul($amount, sprintf('%.8f', $subPrice), 8);
        
        return $mainAmount;
    }


    /**
     * ドル換算を取得
     * @param type $amount
     */
    protected function subToUsd($amount, $subPrice = null)
    {
        $mainAmount = $this->subToMain($amount, $subPrice);
        $usdAmount = $this->mainToUsd($mainAmount);
        
        return $usdAmount;
    }
    
    /**
     * 
     */
    protected function getMarketMerge() {
        
        
        
        $depthList = [
            'USDCUSDT' => 4,
            'BTUCET' => 4,
            'BCHUSDT' => 2,
            'BTCUSDT' => 2,
            'ETHUSDT' => 2,
            'EOSUSDT' => 4,
            'ETCUSDT' => 2,
            'DASHUSDT' => 2,
            'LTCUSDT' => 2,
            'XMRUSDT' => 2,
            'XRPUSDT' => 6,
            'ZECUSDT' => 2,
            'CETUSDT' => 6,
            'SCUSDT' => 6,
            'SEELEUSDT' => 6,
            'OLTUSDT' => 6,
        ];
        if (isset($depthList[$this->market])) {
            $marketMerge = $depthList[$this->market];
        } else {
            $marketMerge = 8;
        }
        
        return $marketMerge;
    }
    
    /**
     * 最小単位を取得
     */
    protected function getMerge()
    {        
        $marketMerge = $this->getMarketMerge();
        
        $merge = '0.' . str_repeat('0', ($marketMerge - 1)) . '1';
        
        return $merge;
    }
    
    /**
     * Order type to price type
     */
    protected function orderTypeToPriceType($orderType)
    {
        if ($orderType === 'buy') {
            $priceType = 'bids';
        } elseif ($orderType === 'sell') {
            $priceType = 'asks';
        } else {
            throw new \Exception('注文タイプがbuy、sellでない');
        }
        
        return $priceType;
    }
    
    
    
    /**
     * Get limit price
     */
    protected function getLimitPrice($orderType)
    {
        $depthData = $this->marketDepthData[$this->market];
        $bidsPrice = sprintf('%.8f', $depthData['bids'][0][0]);
        $asksPrice = sprintf('%.8f', $depthData['asks'][0][0]);
        $referencePrice = bcdiv(bcadd($bidsPrice, $asksPrice, 8), 2, 8);
                
        $adjustmentPrice = bcmul($referencePrice, 0.047, 8);
        
        
        if ($orderType === 'buy') {
            $limitPrice = bcsub($referencePrice, $adjustmentPrice, 8);
        } elseif ($orderType === 'sell') {
            $limitPrice = bcadd($referencePrice, $adjustmentPrice, 8);
        } else {
            
        }        
        
        return $limitPrice;
    }
}