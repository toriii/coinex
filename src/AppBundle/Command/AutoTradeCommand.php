<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Input\InputOption;

/**
 * AutoTradeCommand
 */
class AutoTradeCommand extends BaseBotCommand
{
    /**
     * 注文量
     * @var type 
     */
    private $orderAmount;
    
    /**
     * ポジション
     * @var type 
     */
    private $positions;
    
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('coinex:auto-trade')
            ->setDescription('Auto trade')
            ->addArgument('market', InputArgument::REQUIRED, '対象のペア')
            ->addArgument('order-aamount', InputArgument::REQUIRED, '1注文あたりの枚数')
        ;
    }
    
    
    /**
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
        
        // Marketをセット
        $this->setMarket($this->input->getArgument('market'));
        
        // 注文量
        $this->orderAmount = $this->input->getArgument('order-aamount');
        
        // キャッシュからポジションを取得
        $this->cache = $this->getContainer()->get('cache');
        
        $this->positions = unserialize($this->cache->fetch('positions'));
        var_dump($this->positions);
        exit;
        if (empty($this->positions)) {
            $this->positions = [];
        }
    }
    
    /**
     * Excute
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {       
        do {
            // 残高取得
            $this->updateAccountInfo();
            
            // 板情報を取得
            $this->updateMarketDepthData([$this->market]);
            
            // 買いのためのチェック
            $shoulBuyOrder = $this->shoulBuyOrder();

            // 買い注文
            if ($shoulBuyOrder) {
                $this->buyOrder();
            }
        
            // 売りのためのチェック
            $shoulsSellOrder = $this->shoulSellOrder();
        
            // 売り注文
            $this->sellOrder();
            
            
            // 情報出力
            $this->outputData();
            
            // 待機
            usleep($this->delay);
        } while (true);
        
        
    }
    
    
    
    /**
     * Output data
     */
    private function outputData()
    {
        $this->output->writeln(sprintf('<info>%s (実行間隔: %.1f 秒)</info>', date('Y-m-d H:i:s'), ($this->getDelayM())));
               
        // 板情報
        $this->outputMarketDepthData($this->market, 5, false);
        
        // ポジション
        $this->output->writeln(sprintf('<comment>ポジション状況</comment>'));
        foreach ($this->positions as $position) {
            $price = sprintf('%.8f', $position['price']);
            $now = $this->marketDepthData[$this->market]['last'];
            $diff = bcsub($now, $price, 8);
            $profitMargin = bcdiv($diff * 1000000, $position['price'] * 1000000, 8);
            $outputProfitMargin = ($profitMargin > 0) ? '+' . $profitMargin * 100 :  $profitMargin * 100;
            $this->output->writeln(sprintf('  <info>#%d - %.8f %s - %.2f %s - +%.2f%%</info>', $position['id'], $position['price'], $this->marketMain, $position['amount'], $this->marketSub, $outputProfitMargin));
        }
        
        // 残高
        $this->outputAccountInfo([$this->marketMain, $this->marketSub], false);
        
        $this->output->writeln(sprintf('<comment>%s</comment>', str_repeat('■', date('s'))));
        
        $this->output->writeln('<info>================================================================================</info>');
    }
    
    
    /**
     * 買い注文を出すか判定
     */
    private function shoulBuyOrder()
    {
        $shoulBuyOrder = true;
        
        return $shoulBuyOrder;
    }
    
    /**
     * 売り注文を出すか判定
     */
    private function shoulSellOrder()
    {
        
    }
    
    /**
     * 買い注文
     */
    private function buyOrder()
    {
        $amount = sprintf('%.8f', $this->orderAmount);
        return;
        try {
            $response = $this->api->marketOrder(['market' => $this->market, 'amount' => $amount, 'type' => 'buy']);
            $position = [
                'id' => $response['id'],
                'type' => 'buy',
                'price' => $response['avg_price'],
                'amount' => $response['deal_amount'],
                'created' => date('Y-m-d H:i:s', $response['create_time'])
            ];
            $this->positions[] = $position;
            $this->savePositions();
        } catch (\Exception $ex) {

        }

    }
    
    /**
     * 売り注文
     */
    private function sellOrder()
    {
        
    }
    
    /**
     * Save positions
     */
    private function savePositions()
    {
        $this->cache->save('positions', serialize($this->positions));
    }
    
}
