<?php
namespace AppBundle\Ranking;

class RankingData
{
    private $rankings = [
        19 => [
            'name' => 'Trade BTU & Share 10000000 BTU',
            'start' => '2018-09-16 21:00',
            'end' => '2018-09-23 21:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=133'
        ],
        20 => [
            'name' => 'Trade ULT & Share 1000000 ULT',
            'start' => '2018-09-19 09:00',
            'end' => '2018-09-25 09:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=131'
        ],
        21 => [
            'name' => 'Trade FuzeX  & Share 150,000 FXT',
            'start' => '2018-09-27 12:00',
            'end' => '2018-10-04 12:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=135'
        ],
        22 => [
            'name' => 'LendChain  & Share 500,000 LV',
            'start' => '2018-10-01 13:00',
            'end' => '2018-10-08 13:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=138'
        ],
        23 => [
            'name' => 'ExTrade  & Share 20,000,000 EXTRA',
            'start' => '2018-10-04 12:00',
            'end' => '2018-10-12 12:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=141'
        ],
        24 => [
            'name' => 'ExTrade  & Share 20,000,000 EXTRA',
            'start' => '2018-10-04 12:00',
            'end' => '2018-10-12 12:00',
            'announcement' => 'https://www.coinex.com/announcement/detail?id=141'
        ]
        
    ];
    
    
    public function getRanking($id)
    {
        if (isset($this->rankings[$id])) {
            return $this->rankings[$id];
        }
    }
    
    public function getRankings()
    {
        return $this->rankings;
    }
    
    
}