<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Ranking\RankingData;

class RankingController extends Controller
{
    /**
     * @Route("/rankings")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $rankings = $this->getRankings();
        krsort($rankings);
        
        return compact('rankings');
    }
    
    /**
     * @Route("/ranking/{id}")
     * @Template()
     */
    public function indexAction(Request $request, $id)
    {
        $ranking = $this->getRanking($id);
        if (empty($ranking)) {
            throw $this->createNotFoundException();
        }
        
        $limit = $request->get('limit');
        if (empty($limit)) {
            $limit = 12;
        }
        
        // パス
        $dir = realpath($this->get('kernel')->getRootDir() . '/../web/ranking');
        $data = [];
        for ($i = $limit; $i >= 0; $i--) {
            $updateTime = date('YmdH0000', strtotime('-' . $i . 'Hours'));
            // ファイル名
            $fileName = sprintf('%d_%s.json', $id, $updateTime);
            // ファイルパス
            $filePath = sprintf('%s/%s',$dir, $fileName);
            
            if (!file_exists($filePath)) {
                continue;
            }
            $json = file_get_contents($filePath);
            $rankingData = json_decode($json, true)['data']['rank_info'];
            $displayDate = date('Y-m-d H:00:00', strtotime('-' . $i . 'Hours'));
            $data[$displayDate] = $rankingData;
        }
        return compact('data', 'id', 'ranking');
    }
    
    
    /**
     * @Route("/ranking/{id}/date/{date}")
     * @Template()
     */
    public function dateAction(Request $request, $id, $date)
    {
        $ranking = $this->getRanking($id);
        if (empty($ranking)) {
            throw $this->createNotFoundException();
        }
        $limit = $request->get('limit');
        if (empty($limit)) {
            $limit = 12;
        }
        
        // パス
        $dir = realpath($this->get('kernel')->getRootDir() . '/../web/ranking');
        $data = [];
        // ファイル名
        $fileName = sprintf('%d_%s.json', $id, $date);
        // ファイルパス
        $filePath = sprintf('%s/%s',$dir, $fileName);
           
        if (!file_exists($filePath)) {
            throw $this->createNotFoundException();
        }
        $json = file_get_contents($filePath);
        $data = json_decode($json, true)['data']['rank_info'];
        $updateTime = json_decode($json, true)['data']['update_time'];
        $date = date('Y-m-d H:i:s', $updateTime);
        
        return compact('data', 'date', 'id', 'ranking');
    }
    
    
    /**
     * @Route("/ranking/{id}/user/{user}")
     * @Template()
     */
    public function userAction(Request $request, $id, $user)
    {
        $ranking = $this->getRanking($id);
        if (empty($ranking)) {
            throw $this->createNotFoundException();
        }
        $limit = $request->get('limit');
        if (empty($limit)) {
            $limit = 36;
        }
        
        // パス
        $dir = realpath($this->get('kernel')->getRootDir() . '/../web/ranking');
        $data = [];
        for ($i = $limit; $i >= 0; $i--) {
            $updateTime = date('YmdH0000', strtotime('-' . $i . 'Hours'));
            // ファイル名
            $fileName = sprintf('%d_%s.json', $id, $updateTime);
            // ファイルパス
            $filePath = sprintf('%s/%s',$dir, $fileName);
            
            if (!file_exists($filePath)) {
                continue;
            }
            $json = file_get_contents($filePath);
            $rankingData = json_decode($json, true)['data']['rank_info'];
            $displayDate = date('Y-m-d H:00:00', strtotime('-' . $i . 'Hours'));
            foreach ($rankingData as $val) {
                if ($val[1] == $user) {
                    $userData = ['rank' => $val[0], 'amount' => $val[2]];
                    $data[$displayDate] = $userData;
                    break;
                }
            }
            
        }
        return compact('user', 'id', 'data', 'ranking');
    }
    
    private function getRankings()
    {
        $rankings = (new RankingData())->getRankings();
        
        return $rankings;
    }
    
    private function getRanking($id)
    {
        $ranking = (new RankingData())->getRanking($id);
        
        return $ranking;
    }
    
    
}
